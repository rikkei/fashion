<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>
        @if(isset($title) && $title)
            {{ $title }} - 
        @endif
            Fashion Shop
    </title>

    <!-- Fonts -->
    <link rel="stylesheet" href="{{ URL::asset('css/font-awesome.min.css') }}" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ URL::asset('css/styles.css') }}" />
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}
    
</head>
<body id="app-layout">
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    Fashion
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    {!! \App\Helpers\Menu::getHtml() !!}
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Login</a></li>
                        <li><a href="{{ url('/register') }}">Register</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                    @endif
                    <li>
                        <a href="{{ url('cart') }}">Cart</a>
                    </li>
                    <li>
                        <a href="{{ url('news') }}">News</a>
                    </li>
                    <li>
                        <a href="{{ url('blog') }}">Blog</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    
    <div class="container before-content">
        <!-- Breadcrumb -->
            @include('include.breadcrumb')
        <!-- end Breadcrumb -->
        
        <!-- message alert -->
        @include('messages.success')
        @include('messages.errors')
    </div>
    
    <div class="container content">
        @yield('content')
    </div>
    
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <p class="copyright">&copy; 2016 by GiangSoda</p>
                </div>
            </div>
        </div>
    </div>
    

    <!-- JavaScripts -->
    <script src="{{ URL::asset('js/jquery-2.2.3.min.js') }}"></script>
    <script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('js/script.js') }}"></script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
    @yield('script')
    @yield('scriptCode')
</body>
</html>
