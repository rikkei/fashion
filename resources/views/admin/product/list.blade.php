@extends('admin.layouts.default')

@section('content')
<?php use App\Helpers\Config; ?>
<div class="box-body">
    <div class="dataTables_wrapper form-inline dt-bootstrap">
        <div class="row">
            <div class="col-sm-12">
                <a href="{{ url(config('base.admin_uri').'/product/create') }}"
                   class="btn btn-info">Add new</a>
            </div>
            <div class="col-sm-12">
                <div class="dataTables_filter">
                    <label>Search:
                        <input type="search" class="form-control input-sm" placeholder="Search..." />
                    </label>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-12">
                @include('admin.include.pager')
            </div>
        </div>
        <div class="row">
            <form class="form-grid-actions" method="post" action="{{ url(config('base.admin_uri').'/product/massAction/') }}">
                {!! csrf_field() !!}
                <div class="col-sm-6 grid-filters">
                    <button type="button" class="btn btn-info btn-search-filter"><span>Search</span></button>
                    <button type="button" class="btn btn-info btn-reset-filter" data-href="{{ url(config('base.admin_uri').'/product/') }}">
                        <span>Reset filter</span>
                    </button>
                </div>
                <div class="col-sm-6 grid-actions">
                    <label>Action 
                        <select name="action" class="form-control input-sm">
                            <option value=""></option>
                            <option value="delete">Delete</option>
                        </select>
                    </label>
                    <input type="submit" class="btn btn-info btn-grid-action-submit" name="submit" value="Submit" />
                </div>
                <div class="col-sm-12">
                    <table class="table table-striped dataTable">
                        <thead>
                            <tr>
                                <th>
                                    <input type="checkbox" class="input-checkbox mass-selectbox" />
                                </th>
                                <th class="sorting_asc">Id</th>
                                <th>Name</th>
                                <th>Active</th>
                                <th>Sku</th>
                                <th>Price</th>
                                <th>Qty</th>
                                <th>Stock Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="tr-filter-grid">
                                <td>&nbsp;</td>
                                <td>
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <label>From</label>
                                        </div>
                                        <div class="col-sm-5">
                                            <input type="text" name="filter[id][from]" value="{{ Config::getValueForm(null,'from','filter.id') }}" placeholder="From" class="filter-grid" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <label>To</label>
                                        </div>
                                        <div class="col-sm-5">
                                            <input type="text" name="filter[id][to]" value="{{ Config::getValueForm(null,'to','filter.id') }}" placeholder="To" class="filter-grid" />
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <input type="text" name="filter[name]" value="{{ Config::getValueForm(null,'name', 'filter') }}" placeholder="Name" class="filter-grid" />
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <input type="text" name="filter[active]" value="{{ Config::getValueForm(null,'active', 'filter') }}" placeholder="Active" class="filter-grid" />
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <input type="text" name="filter[sku]" value="{{ Config::getValueForm(null,'sku', 'filter') }}" placeholder="Sku" class="filter-grid" />
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <label>From</label>
                                        </div>
                                        <div class="col-sm-5">
                                            <input type="text" name="filter[price][from]" value="{{ Config::getValueForm(null,'from','filter.price') }}" placeholder="From" class="filter-grid" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <label>To</label>
                                        </div>
                                        <div class="col-sm-5">
                                            <input type="text" name="filter[price][to]" value="{{ Config::getValueForm(null,'to','filter.price') }}" placeholder="To" class="filter-grid" />
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <input type="text" name="filter[qty]" value="{{ Config::getValueForm(null,'qty', 'filter') }}" placeholder="Qty" class="filter-grid" />
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <input type="text" name="filter[stock_status]" value="{{ Config::getValueForm(null,'stock_status', 'filter') }}" placeholder="Stock status" class="filter-grid" />
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @if(count($model))
                                @foreach($model as $item)
                                    <?php $item = \App\Model\Product::loadItem($item->id,['name','price','qty','stock_status',]); ?>
                                    <tr>
                                        <td>
                                            <input type="checkbox" class="input-checkbox item-id mass-item" name="item[]" value="{{ $item->id }}"
                                        </td>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->active }}</td>
                                        <td>{{ $item->sku }}</td>
                                        <td>{{ $item->price }}</td>
                                        <td>{{ $item->qty }}</td>
                                        <td>{{ $item->stock_status }}</td>
                                        <td>
                                            <a href="{{ url(config('base.admin_uri').'/product/edit/'.$item->id) }}" class="button btn-edit">Edit</a>
                                            <span>|</span>
                                            <a href="{{ url(config('base.admin_uri').'/product/delete/'.$item->id.'/'.csrf_token()) }}" class="button btn-delete delete-confirm">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
