<?php use App\Helpers\Config; ?>

<div class="form-group">
    <?php $optionSelected = Config::getValueForm($model,'parent_id','category'); ?>
    @if(count(App\Helpers\Option::categoryTreeCheckbox($categoryProduct)))
        @foreach(App\Helpers\Option::categoryTreeCheckbox($categoryProduct) as $option)
            <div class="row">
                 <div class="col-sm-10">
                    {!! $option['label'] !!}
                </div>
            </div>
        @endforeach
    @endif
    </select>
</div>