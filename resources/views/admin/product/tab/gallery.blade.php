<div class="dataTables_wrapper form-inline dt-bootstrap">
    <div class="row">
        <div class="col-sm-12">
            <table class="table table-striped dataTable table-gallery">
                <thead>
                    <tr>
                        <th>Image</th>
                        <th>Alt</th>
                        <th>Title</th>
                        <th>Position</th>
                        <th>Disable</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    @if(isset($galleryProduct) && count($galleryProduct))
                        @foreach($galleryProduct as $galleryItem)
                            <tr class="exists-gallery">
                                <td>
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <img src="{{ URL::asset($galleryItem->path) }}" 
                                                 width="50" height="50"/>
                                        </div>
                                        <div class="col-sm-2">
                                            <input type="text" name="gallery[{{ $galleryItem->id }}][path]" id="gallery-{{ $galleryItem->id }}" 
                                                   class="form-control" value="{{ $galleryItem->path }}" />
                                        </div>
                                        <div class="col-sm-2">
                                            <button type="button" class="btn btn-info btn-browse-file" data-element="#gallery-{{ $galleryItem->id }}"><span>Browse Image</span></button>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <input type="text" name="gallery[{{ $galleryItem->id }}][alt]"
                                        class="form-control" value="{{ $galleryItem->alt }}" />
                                </td>
                                <td>
                                    <input type="text" name="gallery[{{ $galleryItem->id }}][title]"
                                        class="form-control" value="{{ $galleryItem->title }}" />
                                </td>
                                <td>
                                    <input type="text" name="gallery[{{ $galleryItem->id }}][position]"
                                        class="form-control" value="{{ $galleryItem->position }}" />
                                </td>
                                <td>
                                    <input type="checkbox" name="gallery[{{ $galleryItem->id }}][disable]"
                                           class="input-checkbox" value="1"<?php if($galleryItem->disable): ?> checked<?php endif; ?>/>
                                </td>
                                <td>
                                    <input type="checkbox" name="gallery[{{ $galleryItem->id }}][delete]"
                                        class="input-checkbox" value="1" />
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    <tr class="new-gallery">
                        <td>
                            <div class="row">
                                <div class="col-sm-2">
                                    <input type="text" name="ngallery[0][path]" id="ngallery-0" 
                                           class="form-control" value="" />
                                </div>
                                <div class="col-sm-2">
                                    <button type="button" class="btn btn-info btn-browse-file" data-element="#ngallery-0"><span>Browse Image</span></button>
                                </div>
                            </div>
                        </td>
                        <td>
                            <input type="text" name="ngallery[0][alt]"
                                class="form-control" value="" />
                        </td>
                        <td>
                            <input type="text" name="ngallery[0][title]"
                                class="form-control" value="" />
                        </td>
                        <td>
                            <input type="text" name="ngallery[0][position]"
                                class="form-control" value="" />
                        </td>
                        <td>
                            <input type="checkbox" name="ngallery[0][disable]"
                                class="input-checkbox" value="1" />
                        </td>
                        <td>
                            <input type="checkbox" name="ngallery[0][delete]"
                                class="input-checkbox" value="1" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-sm-12">
            <button type="button" class="btn btn-info btn-gallery-add">
                <span>Add New Gallery</span>
            </button>
        </div>
    </div>
</div>
