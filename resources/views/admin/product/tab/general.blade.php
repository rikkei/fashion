<?php use App\Helpers\Config; ?>

@if(isset($attributes['name']))
<div class="form-group">
    <label class="col-sm-2 control-label" for="item-name">{{ $attributes['name']['label'] }}</label>
    <div class="col-sm-10">
        <input type="text" name="attr[{{ $attributes['name']['id'] }}]" id="item-name" 
               class="form-control" placeholder="Name" value="{{ Config::getValueForm($model,$attributes['name']['id'], 'attr', 'name') }}" />
    </div>
</div>
@endif


<div class="form-group">
    <label class="col-sm-2 control-label" for="item-sku">Sku</label>
    <div class="col-sm-10">
        <input type="text" name="item[sku]" id="item-sku" 
               class="form-control" placeholder="Sku" value="{{ Config::getValueForm($model, 'sku', 'item') }}" />
    </div>
</div>


@if(isset($attributes['url_key']))
<div class="form-group">
    <label class="col-sm-2 control-label" for="item-url_key">{{ $attributes['url_key']['label'] }}</label>
    <div class="col-sm-10">
        <input type="text" name="attr[{{ $attributes['url_key']['id'] }}]" id="item-url_key" 
               class="form-control" placeholder="Url Key" value="{{ Config::getValueForm($model, $attributes['url_key']['id'], 'attr', 'url_key') }}" />
    </div>
</div>
@endif

<div class="form-group">
    <label class="col-sm-2 control-label" for="item-active">Active</label>
    <div class="col-sm-10">
        <?php $optionSelected = Config::getValueForm($model,'active','item'); ?>
        <select name="item[active]" id="item-active" 
                class="form-control">
            @foreach(App\Helpers\Option::yesNo() as $option)
            <option value="{{ $option['value'] }}"
                <?php if($optionSelected == $option['value']) echo ' selected'; ?>
            >{{ $option['label'] }}</option>
            @endforeach
        </select>
    </div>
</div>

@if(isset($attributes['short_description']))
<div class="form-group">
    <label class="col-sm-2 control-label" for="item-short-description">{{ $attributes['short_description']['label'] }}</label>
    <div class="col-sm-10">
        <textarea name="attr[{{ $attributes['short_description']['id'] }}]" id="item-short-description" 
                  class="form-control" placeholder="Short Description">{{ Config::getValueForm($model, $attributes['short_description']['id'],'attr', 'short_description') }}</textarea>
    </div>
</div>
@endif

@if(isset($attributes['description']))
<div class="form-group">
    <label class="col-sm-2 control-label" for="item-description">{{ $attributes['description']['label'] }}</label>
    <div class="col-sm-10">
        <textarea name="attr[{{ $attributes['description']['id'] }}]" id="item-description" 
                  class="form-control" placeholder="Description">{{ Config::getValueForm($model, $attributes['description']['id'], 'attr', 'description') }}</textarea>
    </div>
</div>
@endif


@if(isset($attributes['price']))
<div class="form-group">
    <label class="col-sm-2 control-label" for="item-price">{{ $attributes['price']['label'] }}</label>
    <div class="col-sm-10">
        <input name="attr[{{ $attributes['price']['id'] }}]" id="item-price" 
                  class="form-control" placeholder="Price" value="{{ Config::getValueForm($model, $attributes['price']['id'], 'attr', 'price') }}" />
    </div>
</div>
@endif

@if(isset($attributes['special_price']))
<div class="form-group">
    <label class="col-sm-2 control-label" for="item-special_price">{{ $attributes['special_price']['label'] }}</label>
    <div class="col-sm-10">
        <input name="attr[{{ $attributes['special_price']['id'] }}]" id="item-special_price" 
                  class="form-control" placeholder="Spcial Price" value="{{ Config::getValueForm($model, $attributes['special_price']['id'], 'attr', 'special_price') }}" />
    </div>
</div>
@endif

@if(isset($attributes['special_price_from']))
<div class="form-group">
    <label class="col-sm-2 control-label" for="item-special_price_from">{{ $attributes['special_price_from']['label'] }}</label>
    <div class="col-sm-10">
        <input name="attr[{{ $attributes['special_price_from']['id'] }}]" id="item-special_price_from" 
               class="form-control" placeholder="Special Price From" value="{{ Config::getValueForm($model, $attributes['special_price_from']['id'], 'attr', 'special_price_from') }}" />
    </div>
</div>
@endif

@if(isset($attributes['special_price_to']))
<div class="form-group">
    <label class="col-sm-2 control-label" for="item-special_price_to">{{ $attributes['special_price_to']['label'] }}</label>
    <div class="col-sm-10">
        <input name="attr[{{ $attributes['special_price_to']['id'] }}]" id="item-special_price_to" 
                  class="form-control" placeholder="Special Price To" value="{{ Config::getValueForm($model, $attributes['special_price_to']['id'], 'attr', 'special_price_to') }}" />
    </div>
</div>
@endif

@if(isset($attributes['qty']))
<div class="form-group">
    <label class="col-sm-2 control-label" for="item-qty">{{ $attributes['qty']['label'] }}</label>
    <div class="col-sm-10">
        <input name="attr[{{ $attributes['qty']['id'] }}]" id="item-qty" 
                  class="form-control" placeholder="Qty" value="{{ Config::getValueForm($model, $attributes['qty']['id'] , 'attr', 'qty') }}" />
    </div>
</div>
@endif

@if(isset($attributes['stock_status']))
<div class="form-group">
    <label class="col-sm-2 control-label" for="item-stock_status">{{ $attributes['stock_status']['label'] }}</label>
    <div class="col-sm-10">
        <?php $optionSelected = Config::getValueForm($model, $attributes['stock_status']['id'] , 'attr', 'stock_status'); ?>
        <select name="attr[{{ $attributes['stock_status']['id'] }}]" id="item-stock_status" 
                class="form-control">
            @foreach(App\Helpers\Option::yesNo() as $option)
            <option value="{{ $option['value'] }}"
                <?php if($optionSelected == $option['value']) echo ' selected'; ?>
            >{{ $option['label'] }}</option>
            @endforeach
        </select>
    </div>
</div>
@endif
