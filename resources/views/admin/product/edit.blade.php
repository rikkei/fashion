@extends('admin.layouts.default')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="form-wrapper">
            <form class="form-horizontal" action="" method="post" enctype='multipart/form-data'>
                {!! csrf_field() !!}
                <div class="box-footer">
                    <input type="submit" class="btn btn-default" name="submit" value="Save" />
                    <input type="submit" class="btn btn-info" name="submit_continue" value="Save And Continue" />
                    <input type="reset" class="btn btn-info" name="reset" value="Reset" />
                    @if(isset($model) && $model->id)
                        <a href="{{ url(config('base.admin_uri').'/product/delete/'.$model->id.'/'.csrf_token()) }}" class="btn btn-danger delete-confirm" >
                            Delete
                        </a>
                    @endif
                </div>
                <div class="box-body">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#item-general">General</a></li>
                        <li><a data-toggle="tab" href="#item-category">Category</a></li>
                        <li><a data-toggle="tab" href="#item-gallery">Gallery</a></li>
                    </ul>
                    <div class="tab-content">
                        <!-- Tab General Category -->
                        <div id="item-general" class="tab-pane active">
                            @include('admin.product.tab.general')
                        </div>
                        
                        <!-- Tab Products Gallery -->
                        <div id="item-category" class="tab-pane">
                            @include('admin.product.tab.category')
                        </div>
                        
                        <div id="item-gallery" class="tab-pane">
                            @include('admin.product.tab.gallery')
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('scriptCode')
<script>
    jQuery(document).ready(function ($) {
        CKEDITOR.replace( 'item-short-description');
        CKEDITOR.replace( 'item-description');
        $(document).on('click','.btn-browse-file',function(event) {
            var id = $(this).data('element');
            if(!id || !$(id).length) {
                return false;
            }
            var finder = new CKFinder();
            finder.selectActionFunction = function(fileUrl) {
                $(id).val(fileUrl);
            };
            finder.popup();
        });
        var galleryNewHtml = $('tr.new-gallery')[0].outerHTML,
            indexNew = 1;
        $('.btn-gallery-add').click(function(event) {
            var galleryProcess = galleryNewHtml.replace(/name="ngallery\[0\]/g,'name="ngallery['+indexNew+']');
            galleryProcess = galleryProcess.replace(/id="ngallery-0"/g,'id="ngallery-'+indexNew+'"');
            galleryProcess = galleryProcess.replace(/data-element="#ngallery-0"/g,'data-element="#ngallery-'+indexNew+'"');
            indexNew++;
            $('table.table-gallery tbody').append(galleryProcess);
        });
    });
</script>
@endsection
