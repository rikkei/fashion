<ul class="sidebar-menu">
    <li class="header">MAIN NAVIGATION</li>
    <li class="active treeview">
        <a href="{{ url(config('base.admin_uri')) }}">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a>
    </li>
    <li class="treeview">
        <a href="{{ url(config('base.admin_uri').'/order') }}">
            <i class="fa fa-files-o"></i>
            <span>Order</span>
        </a>
    </li>
    <li class="treeview">
        <a href="{{ url(config('base.admin_uri').'/category') }}">
            <i class="fa fa-files-o"></i>
            <span>Category</span>
        </a>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Product</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li><a href="{{ url(config('base.admin_uri').'/product/create') }}"><i class="fa fa-circle-o"></i> Add new</a></li>
            <li><a href="{{ url(config('base.admin_uri').'/product/') }}"><i class="fa fa-circle-o"></i> View List</a></li>
        </ul>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-files-o"></i>
            <span>News</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li><a href="{{ url(config('base.admin_uri').'/news/create') }}"><i class="fa fa-circle-o"></i> Add new</a></li>
            <li><a href="{{ url(config('base.admin_uri').'/news/') }}"><i class="fa fa-circle-o"></i> View List</a></li>
        </ul>
    </li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Blog</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li><a href="{{ url(config('base.admin_uri').'/blog') }}"><i class="fa fa-circle-o"></i> View Blog</a></li>
            <li><a href="{{ url(config('base.admin_uri').'/blog/category') }}"><i class="fa fa-circle-o"></i> View Category</a></li>
            <li><a href="{{ url(config('base.admin_uri').'/blog/comment') }}"><i class="fa fa-circle-o"></i> View Comment</a></li>
        </ul>
    </li>
    <li class="treeview">
        <a href="{{ url(config('base.admin_uri').'/url/rewrite') }}">
            <i class="fa fa-files-o"></i>
            <span>Url Rewrite</span>
        </a>
    </li>
</ul>