@if(count($items))
<div class="row">
    <div class="col-sm-12">
        <table class="table table-striped dataTable">
            <thead>
                <tr>
                    <th>Product Id</th>
                    <th>Name</th>
                    <th>Sku</th>
                    <th>Unit Price</th>
                    <th>Qty</th>
                    <th>Subtotal</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($items as $item)
                    <tr>
                        <td>
                            {{ $item->product_id }}
                        </td>
                        <td>
                            {{ $item->name }}
                        </td>
                        <td>
                            {{ $item->sku }}
                        </td>
                        <td>
                            {!! \App\Helpers\Output::price($item->price) !!}
                        </td>
                        <td>
                            {{ $item->qty }}
                        </td>
                        <td>
                            {!! \App\Helpers\Output::price($item->price * $item->qty) !!}
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endif