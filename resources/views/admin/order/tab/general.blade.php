<?php use App\Helpers\Config; ?>

<div class="form-group">
    <label class="col-sm-2 control-label" for="item-name">Id</label>
    <div class="col-sm-10">
        <span>{{ $model->id }}</span>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label" for="item-name">Created At</label>
    <div class="col-sm-10">
        <span>{{ $model->created_at }}</span>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label" for="item-name">Stauts</label>
    <div class="col-sm-10">
        <span>{{ $model->status }}</span>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label" for="item-name">User name</label>
    <div class="col-sm-10">
        <span>{{ $model->user_name }}</span>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label" for="item-name">User Email</label>
    <div class="col-sm-10">
        <span>{{ $model->user_email }}</span>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label" for="item-name">User Phone</label>
    <div class="col-sm-10">
        <span>{{ $model->user_phone }}</span>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label" for="item-name">Address</label>
    <div class="col-sm-10">
        <span>{!! $model->user_address !!}</span>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label" for="item-name">Grand Total</label>
    <div class="col-sm-10">
        <span>{{ $model->grandtotal }}</span>
    </div>
</div>