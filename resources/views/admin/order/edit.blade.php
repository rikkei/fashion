@extends('admin.layouts.default')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="form-wrapper">
            <form class="form-horizontal" action="" method="post" enctype='multipart/form-data'>
                <div class="box-body">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#item-general">General</a></li>
                        <li><a data-toggle="tab" href="#item-items">Items</a></li>
                    </ul>
                    <div class="tab-content">
                        <!-- Tab General Category -->
                        <div id="item-general" class="tab-pane active">
                            @include('admin.order.tab.general')
                        </div>
                        
                        <!-- Tab Products Gallery -->
                        <div id="item-items" class="tab-pane">
                            @include('admin.order.tab.items')
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
