<?php use App\Helpers\Config; ?>

<div class="form-group">
    <label class="col-sm-2 control-label" for="item-sku">Name</label>
    <div class="col-sm-10">
        <input type="text" name="item[name]" id="item-name" 
               class="form-control" placeholder="Name" value="{{ Config::getValueForm($model, 'name', 'item') }}" />
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label" for="item-url_key">url key</label>
    <div class="col-sm-10">
        <input type="text" name="item[url_key]" id="item-url_key" 
               class="form-control" placeholder="url key" value="{{ Config::getValueForm($model, 'url_key', 'item') }}" />
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label" for="item-active">Active</label>
    <div class="col-sm-10">
        <?php $optionSelected = Config::getValueForm($model,'active','item'); ?>
        <select name="item[active]" id="item-active" 
                class="form-control">
            @foreach(App\Helpers\Option::yesNo() as $option)
            <option value="{{ $option['value'] }}"
                <?php if($optionSelected == $option['value']) echo ' selected'; ?>
            >{{ $option['label'] }}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label" for="item-thumbnail">Thumbnail</label>
    <div class="col-sm-10">
        <div class="row">
            <?php $optionImage = Config::getValueForm($model,'thumbnail','item'); ?>
            @if($optionImage)
                <div class="col-sm-2">
                    <a href="{{ URL::asset($optionImage) }}" target="_blank">
                        <img src="{{ URL::asset($optionImage) }}" width="50" height="50" />
                    </a>
                </div>
            @endif
            <div class="col-sm-6">
                <input type="text" name="item[thumbnail]" id="item-thumbnail" 
                       class="form-control" value="{{ $optionImage }}" />
            </div>
            <div class="col-sm-2">
                <button type="button" class="btn btn-info btn-browse-file" data-element="#item-thumbnail"><span>Browse Image</span></button>
            </div>
        </div>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label" for="item-image">Image</label>
    <div class="col-sm-10">
        <div class="row">
            <?php $optionImage = Config::getValueForm($model,'image','item'); ?>
            @if($optionImage)
                <div class="col-sm-2">
                    <a href="{{ URL::asset($optionImage) }}" target="_blank">
                        <img src="{{ URL::asset($optionImage) }}" width="50" height="50" />
                    </a>
                </div>
            @endif
            <div class="col-sm-6">
                <input type="text" name="item[image]" id="item-image" 
                       class="form-control" value="{{ $optionImage }}" />
            </div>
            <div class="col-sm-2">
                <button type="button" class="btn btn-info btn-browse-file" data-element="#item-image"><span>Browse Image</span></button>
            </div>
        </div>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label" for="item-short_description">Short Description</label>
    <div class="col-sm-10">
        <textarea name="item[short_description]" id="item-short_description" 
                  class="form-control" placeholder="Short Description">{{ Config::getValueForm($model, 'short_description', 'item') }}</textarea>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label" for="item-description">Description</label>
    <div class="col-sm-10">
        <textarea name="item[description]" id="item-description" 
            class="form-control" placeholder="Description">{{ Config::getValueForm($model, 'description', 'item') }}</textarea>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label" for="item-created_at">Created At</label>
    <div class="col-sm-5">
        <div class='input-group date' id='datetimepicker-created_at'>
            <input type='text' name="item[created_at]" id="item-created_at" 
                class="form-control" placeholder="Create At"
                value="{{ Config::getValueForm($model, 'created_at', 'item') }}"/>
            <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
            </span>
        </div>
    </div>
</div>
