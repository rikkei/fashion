@extends('admin.layouts.default')

@section('content')
<?php 
use App\Helpers\Config;
?>
<div class="box-body">
    <div class="dataTables_wrapper form-inline dt-bootstrap">
        <div class="row">
            <div class="col-sm-12">
                <a href="{{ url(config('base.admin_uri').'/news/create') }}"
                   class="btn btn-info">Add new</a>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-12">
                @include('admin.include.pager')
            </div>
        </div>
        <div class="row">
            <form class="form-grid-actions" method="post" action="{{ url(config('base.admin_uri').'/news/massAction/') }}">
                {!! csrf_field() !!}
                <div class="col-sm-6 grid-filters">
                    <button type="button" class="btn btn-info btn-search-filter"><span>Search</span></button>
                    <button type="button" class="btn btn-info btn-reset-filter" data-href="{{ url(config('base.admin_uri').'/news/') }}">
                        <span>Reset filter</span>
                    </button>
                </div>
                <div class="col-sm-6 grid-actions">
                    <label>Action 
                        <select name="action" class="form-control input-sm">
                            <option value=""></option>
                            <option value="delete">Delete</option>
                        </select>
                    </label>
                    <input type="submit" class="btn btn-info btn-grid-action-submit" name="submit" value="Submit" />
                </div>
                <div class="col-sm-12">
                    <table class="table table-striped dataTable">
                        <thead>
                            <tr>
                                <th>
                                    <input type="checkbox" class="input-checkbox mass-selectbox" />
                                </th>
                                <th class="sortable {{ Config::getDirClass('id') }}" onclick="window.location.href='{{Config::getUrlOrder('id')}}';">Id</th>
                                <th class="sortable {{ Config::getDirClass('name') }}" onclick="window.location.href='{{Config::getUrlOrder('name')}}';">Name</th>
                                <th>Url Key</th>
                                <th class="sortable {{ Config::getDirClass('active') }}" onclick="window.location.href='{{Config::getUrlOrder('active')}}';">Active</th>
                                <th class="sortable {{ Config::getDirClass('created_at') }}" onclick="window.location.href='{{Config::getUrlOrder('created_at')}}';">Created At</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="tr-filter-grid">
                                <td>&nbsp;</td>
                                <td>
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <label>From</label>
                                        </div>
                                        <div class="col-sm-5">
                                            <input type="text" name="filter[id][from]" value="{{ Config::getValueForm(null,'from','filter.id') }}" placeholder="From" class="filter-grid" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <label>To</label>
                                        </div>
                                        <div class="col-sm-5">
                                            <input type="text" name="filter[id][to]" value="{{ Config::getValueForm(null,'to','filter.id') }}" placeholder="To" class="filter-grid" />
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <input type="text" name="filter[name]" value="{{ Config::getValueForm(null,'name', 'filter') }}" placeholder="Name" class="filter-grid" />
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <input type="text" name="filter[url_key]" value="{{ Config::getValueForm(null,'url_key', 'filter') }}" placeholder="Url Key" class="filter-grid" />
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <input type="text" name="filter[active]" value="{{ Config::getValueForm(null,'active', 'filter') }}" placeholder="Active" class="filter-grid" />
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <input type="text" name="filter[created_at]" value="{{ Config::getValueForm(null,'created_at', 'filter') }}" placeholder="Qty" class="filter-grid" />
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @if(count($model))
                                @foreach($model as $item)
                                    <tr>
                                        <td>
                                            <input type="checkbox" class="input-checkbox item-id mass-item" name="item[]" value="{{ $item->id }}"
                                        </td>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->url_key }}</td>
                                        <td>{{ $item->active }}</td>
                                        <td>{{ $item->created_at }}</td>
                                        <td>
                                            <a href="{{ url(config('base.admin_uri').'/news/edit/'.$item->id) }}" class="button btn-edit">Edit</a>
                                            <span>|</span>
                                            <a href="{{ url(config('base.admin_uri').'/news/delete/'.$item->id.'/'.csrf_token()) }}" class="button btn-delete delete-confirm">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
