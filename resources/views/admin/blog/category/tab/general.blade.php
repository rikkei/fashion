<?php use App\Helpers\Config; ?>

<div class="form-group">
    <label class="col-sm-2 control-label" for="item-sku">Name</label>
    <div class="col-sm-10">
        <input type="text" name="item[name]" id="item-name" 
               class="form-control" placeholder="Name" value="{{ Config::getValueForm($model, 'name', 'item') }}" />
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label" for="item-url_key">Url key</label>
    <div class="col-sm-10">
        <input type="text" name="item[url_key]" id="item-url_key" 
               class="form-control" placeholder="Url key" value="{{ Config::getValueForm($model, 'url_key', 'item') }}" />
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label" for="item-active">Active</label>
    <div class="col-sm-10">
        <?php $optionSelected = Config::getValueForm($model,'active','item'); ?>
        <select name="item[active]" id="item-active" 
                class="form-control">
            @foreach(App\Helpers\Option::yesNo() as $option)
            <option value="{{ $option['value'] }}"
                <?php if($optionSelected == $option['value']) echo ' selected'; ?>
            >{{ $option['label'] }}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label" for="item-position">Position</label>
    <div class="col-sm-10">
        <input type="text" name="item[position]" id="item-position" 
               class="form-control" placeholder="position" value="{{ Config::getValueForm($model, 'position', 'item') }}" />
    </div>
</div>
