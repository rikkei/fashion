@extends('admin.layouts.default')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="form-wrapper">
            <form class="form-horizontal" action="" method="post" enctype='multipart/form-data'>
                {!! csrf_field() !!}
                <div class="box-footer">
                    <input type="submit" class="btn btn-default" name="submit" value="Save" />
                    <input type="submit" class="btn btn-info" name="submit_continue" value="Save And Continue" />
                    <input type="reset" class="btn btn-info" name="reset" value="Reset" />
                    @if(isset($model) && $model->id)
                        <a href="{{ url(config('base.admin_uri').'/blog/category/delete/'.$model->id.'/'.csrf_token()) }}" class="btn btn-danger delete-confirm" >
                            Delete
                        </a>
                    @endif
                </div>
                <div class="box-body">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#item-general">General</a></li>
                    </ul>
                    <div class="tab-content">
                        <!-- Tab General Category -->
                        <div id="item-general" class="tab-pane active">
                            @include('admin.blog.category.tab.general')
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
