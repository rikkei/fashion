<?php use App\Helpers\Config; ?>

<div class="form-group">
    <label class="col-sm-2 control-label" for="item-sku">User Id</label>
    <div class="col-sm-10">
        <span>{{ Config::getValueForm($model, 'user_id', 'item') }}</span>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label" for="item-sku">Blog Id</label>
    <div class="col-sm-10">
        <span>{{ Config::getValueForm($model, 'blog_id', 'item') }}</span>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label" for="item-sku">Parent Id</label>
    <div class="col-sm-10">
        <span>{{ Config::getValueForm($model, 'parent_id', 'item') }}</span>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label" for="item-approve">Approve</label>
    <div class="col-sm-10">
        <?php $optionSelected = Config::getValueForm($model,'approve','item'); ?>
        <select name="item[approve]" id="item-approve" 
                class="form-control">
            @foreach(App\Helpers\Option::yesNo() as $option)
            <option value="{{ $option['value'] }}"
                <?php if($optionSelected == $option['value']) echo ' selected'; ?>
            >{{ $option['label'] }}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label" for="item-sku">Created At</label>
    <div class="col-sm-10">
        <span>{{ Config::getValueForm($model, 'created_at', 'item') }}</span>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label" for="item-content">Content</label>
    <div class="col-sm-10">
        <textarea name="item[content]" id="item-content" 
                  class="form-control" placeholder="Content">{{ Config::getValueForm($model,'content','content') }}</textarea>
    </div>
</div>