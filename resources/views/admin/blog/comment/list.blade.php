@extends('admin.layouts.default')

@section('content')
<?php use App\Helpers\Config; ?>
<div class="box-body">
    <div class="dataTables_wrapper form-inline dt-bootstrap">
        <div class="row">
            <div class="col-sm-12">
                @include('admin.include.pager')
            </div>
        </div>
        <div class="row">
            <form class="form-grid-actions" method="post" action="{{ url(config('base.admin_uri').'/blog/comment/massAction/') }}">
                {!! csrf_field() !!}
                <div class="col-sm-6 grid-filters">
                    <button type="button" class="btn btn-info btn-search-filter"><span>Search</span></button>
                    <button type="button" class="btn btn-info btn-reset-filter" data-href="{{ url(config('base.admin_uri').'/blog/comment/') }}">
                        <span>Reset filter</span>
                    </button>
                </div>
                <div class="col-sm-6 grid-actions">
                    <label>Action 
                        <select name="action" class="form-control input-sm">
                            <option value=""></option>
                            <option value="delete">Delete</option>
                        </select>
                    </label>
                    <input type="submit" class="btn btn-info btn-grid-action-submit" name="submit" value="Submit" />
                </div>
                <div class="col-sm-12">
                    <table class="table table-striped dataTable">
                        <thead>
                            <tr>
                                <th>
                                    <input type="checkbox" class="input-checkbox mass-selectbox" />
                                </th>
                                <th class="sortable {{ Config::getDirClass('id') }}" onclick="window.location.href='{{Config::getUrlOrder('id')}}';">Id</th>
                                <th class="sortable {{ Config::getDirClass('blog_id') }}" onclick="window.location.href='{{Config::getUrlOrder('blog_id')}}';">Blog id</th>
                                <th class="sortable {{ Config::getDirClass('user_id') }}" onclick="window.location.href='{{Config::getUrlOrder('user_id')}}';">User Id</th>
                                <th class="sortable {{ Config::getDirClass('parent_id') }}" onclick="window.location.href='{{Config::getUrlOrder('parent_id')}}';">Parent id</th>
                                <th class="sortable {{ Config::getDirClass('approve') }}" onclick="window.location.href='{{Config::getUrlOrder('approve')}}';">Approve</th>
                                <th class="sortable {{ Config::getDirClass('created_at') }}" onclick="window.location.href='{{Config::getUrlOrder('created_at')}}';">Created At</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="tr-filter-grid">
                                <td>&nbsp;</td>
                                <td>
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <label>From</label>
                                        </div>
                                        <div class="col-sm-5">
                                            <input type="text" name="filter[id][from]" value="{{ Config::getValueForm(null,'from','filter.id') }}" placeholder="From" class="filter-grid" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <label>To</label>
                                        </div>
                                        <div class="col-sm-5">
                                            <input type="text" name="filter[id][to]" value="{{ Config::getValueForm(null,'to','filter.id') }}" placeholder="To" class="filter-grid" />
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <label>From</label>
                                        </div>
                                        <div class="col-sm-5">
                                            <input type="text" name="filter[blog_id][from]" value="{{ Config::getValueForm(null,'from','filter.blog_id') }}" placeholder="From" class="filter-grid" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <label>To</label>
                                        </div>
                                        <div class="col-sm-5">
                                            <input type="text" name="filter[blog_id][to]" value="{{ Config::getValueForm(null,'to','filter.blog_id') }}" placeholder="To" class="filter-grid" />
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <label>From</label>
                                        </div>
                                        <div class="col-sm-5">
                                            <input type="text" name="filter[user_id][from]" value="{{ Config::getValueForm(null,'from','filter.user_id') }}" placeholder="From" class="filter-grid" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <label>To</label>
                                        </div>
                                        <div class="col-sm-5">
                                            <input type="text" name="filter[user_id][to]" value="{{ Config::getValueForm(null,'to','filter.user_id') }}" placeholder="To" class="filter-grid" />
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <label>From</label>
                                        </div>
                                        <div class="col-sm-5">
                                            <input type="text" name="filter[parent_id][from]" value="{{ Config::getValueForm(null,'from','filter.parent_id') }}" placeholder="From" class="filter-grid" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <label>To</label>
                                        </div>
                                        <div class="col-sm-5">
                                            <input type="text" name="filter[parent_id][to]" value="{{ Config::getValueForm(null,'to','filter.parent_id') }}" placeholder="To" class="filter-grid" />
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <input type="text" name="filter[approve]" value="{{ Config::getValueForm(null,'approve', 'filter') }}" placeholder="Approve" class="filter-grid" />
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <input type="text" name="filter[created_at]" value="{{ Config::getValueForm(null,'created_at', 'filter') }}" placeholder="Created At" class="filter-grid" />
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @if(count($model))
                                @foreach($model as $item)
                                    <tr>
                                        <td>
                                            <input type="checkbox" class="input-checkbox item-id mass-item" name="item[]" value="{{ $item->id }}"
                                        </td>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->blog_id }}</td>
                                        <td>{{ $item->user_id }}</td>
                                        <td>{{ $item->parent_id }}</td>
                                        <td>{{ $item->approve }}</td>
                                        <td>{{ $item->created_at }}</td>
                                        <td>
                                            <a href="{{ url(config('base.admin_uri').'/blog/comment/edit/'.$item->id) }}" class="button btn-edit">Edit</a>
                                            <span>|</span>
                                            <a href="{{ url(config('base.admin_uri').'/blog/comment/delete/'.$item->id.'/'.csrf_token()) }}" class="button btn-delete delete-confirm">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
