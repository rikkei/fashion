<?php
$categoryTree = \App\Helpers\Option::getTreeFull();
if (!count($categoryTree)) {
    return;
}
?>
@foreach($categoryTree as $item)
    <div class="tree-item">
        <a href="{{ url(config('base.admin_uri') . '/category/edit/'. $item['value']) }}">
            <span>{{ $item['label'] }}</span>
        </a>
    </div>
@endforeach