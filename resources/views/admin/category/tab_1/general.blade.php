<?php use App\Helpers\Config; ?>

<div class="form-group">
    <label class="col-sm-2 control-label" for="category-name">Name</label>
    <div class="col-sm-10">
        <input type="text" name="category[name]" id="category-name" 
               class="form-control" placeholder="name" value="{{ Config::getValueForm($model,'name','category') }}" />
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label" for="category-url_key">Url Key</label>
    <div class="col-sm-10">
        <input type="text" name="category[url_key]" id="category-url_key" 
               class="form-control" placeholder="Url Key" value="{{ Config::getValueForm($model,'url_key','category') }}" />
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label" for="category-active">Active</label>
    <div class="col-sm-10">
        <?php $optionSelected = Config::getValueForm($model,'active','category'); ?>
        <select name="category[active]" id="category-active" 
                class="form-control">
            @foreach(App\Helpers\Option::yesNo() as $option)
            <option value="{{ $option['value'] }}"
                <?php if($optionSelected == $option['value']) echo ' selected'; ?>
            >{{ $option['label'] }}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label" for="category-image">Image</label>
    <div class="col-sm-10">
        <div class="row">
            <?php $optionImage = Config::getValueForm($model,'image','category'); ?>
            @if($optionImage)
                <div class="col-sm-2">
                    <a href="{{ URL::asset($optionImage) }}" target="_blank">
                        <img src="{{ URL::asset($optionImage) }}" width="50" height="50" />
                    </a>
                </div>
            @endif
            <div class="col-sm-6">
                <input type="text" name="category[image]" id="category-image" 
                       class="form-control" value="{{ $optionImage }}" />
            </div>
            <div class="col-sm-2">
                <button type="button" class="btn btn-info btn-browse-file" data-element="#category-image"><span>Browse Image</span></button>
            </div>
        </div>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label" for="category-description">Description</label>
    <div class="col-sm-10">
        <textarea name="category[description]" id="category-description" 
                  class="form-control" placeholder="Description" rows="4">{{ Config::getValueForm($model,'description','category') }}</textarea>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label" for="category-parent_id">Parent</label>
    <div class="col-sm-10">
        <?php $optionSelected = Config::getValueForm($model,'parent_id','category'); ?>
        <select name="category[parent_id]" id="category-parent_id" 
                class="form-control">
            @if(count(App\Helpers\Option::categoryTree()))
                @foreach(App\Helpers\Option::categoryTree() as $option)
                    <option value="{{ $option['value'] }}"
                        <?php if($optionSelected == $option['value']) echo ' selected'; ?>
                    >{{ $option['label'] }}</option>
                @endforeach
            @endif
        </select>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label" for="category-position">Position</label>
    <div class="col-sm-10">
        <input type="text" name="category[position]" id="category-position" 
               class="form-control" placeholder="Position" value="{{ Config::getValueForm($model,'position','category') }}" />
    </div>
</div>