@extends('admin.layouts.default')

@section('content')
<?php use App\Helpers\Config; ?>
<div class="box-body">
    <div class="dataTables_wrapper dt-bootstrap">
        <div class="row">
            <div class="col-sm-12">
                <a href="{{ url(config('base.admin_uri').'/category') }}"
                   class="btn btn-info">Add new</a>
            </div>
        </div>
        
        <div class="row category-content">
            <div class="col-sm-3 category-tree">
                <div class="category-tree-inner">
                    @include('admin.category.tree')
                </div>
            </div>
            <div class="col-sm-9 category-view">
                @include('admin.category.form')
            </div>
        </div>
    </div>
</div>
@endsection

@section('scriptCode')
<script>
    jQuery(document).ready(function ($) {
        CKEDITOR.replace( 'category-description');
        $('.btn-browse-file').click(function(event) {
            var id = $(this).data('element');
            if(!id || !$(id).length) {
                return false;
            }
            var finder = new CKFinder();
            finder.selectActionFunction = function(fileUrl) {
                $(id).val(fileUrl);
            };
            finder.popup();
        });
    });
</script>
@endsection