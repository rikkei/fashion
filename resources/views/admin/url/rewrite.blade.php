@extends('admin.layouts.default')

@section('content')
    <?php use App\Helpers\Config; ?>
    <div class="box-body">
        <div class="dataTables_wrapper form-inline dt-bootstrap">
            <div class="row">
                <div class="col-sm-12">
                    <a href="{{ url(config('base.admin_uri').'/url/rewrite/reindex/'.csrf_token()) }}"
                       class="btn btn-danger delete-confirm" data-message="Are you reindex url">Reindex Url</a>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    @include('admin.include.pager')
                </div>
            </div>
            <div class="row">
                <form class="form-grid-actions" method="post" action="{{ url(config('base.admin_uri').'/url/rewrite/massAction/') }}">
                    {!! csrf_field() !!}
                    <div class="col-sm-6 grid-filters">
                        <button type="button" class="btn btn-info btn-search-filter"><span>Search</span></button>
                        <button type="button" class="btn btn-info btn-reset-filter" data-href="{{ url(config('base.admin_uri').'/url/rewrite/') }}">
                            <span>Reset filter</span>
                        </button>
                    </div>
                    <div class="col-sm-6 grid-actions">
                        <label>Action
                            <select name="action" class="form-control input-sm">
                                <option value=""></option>
                                <option value="delete">Delete</option>
                            </select>
                        </label>
                        <input type="submit" class="btn btn-info btn-grid-action-submit" name="submit" value="Submit" />
                    </div>
                    <div class="col-sm-12">
                        <table class="table table-striped dataTable">
                            <thead>
                            <tr>
                                <th>
                                    <input type="checkbox" class="input-checkbox mass-selectbox" />
                                </th>
                                <th class="sorting_asc">Id</th>
                                <th>Type</th>
                                <th>Request Path</th>
                                <th>Target Path</th>
                                <th>Params</th>
                                <th>Redirect</th>
                                <th>&nbsp;</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="tr-filter-grid">
                                <td>&nbsp;</td>
                                <td>
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <label>From</label>
                                        </div>
                                        <div class="col-sm-5">
                                            <input type="text" name="filter[id][from]" value="{{ Config::getValueForm(null,'from','filter.id') }}" placeholder="From" class="filter-grid" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <label>To</label>
                                        </div>
                                        <div class="col-sm-5">
                                            <input type="text" name="filter[id][to]" value="{{ Config::getValueForm(null,'to','filter.id') }}" placeholder="To" class="filter-grid" />
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <input type="text" name="filter[type]" value="{{ Config::getValueForm(null,'type', 'filter') }}" placeholder="Type" class="filter-grid" />
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <input type="text" name="filter[request_path]" value="{{ Config::getValueForm(null,'request_path', 'filter') }}" placeholder="Request Path" class="filter-grid" />
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <input type="text" name="filter[target_path]" value="{{ Config::getValueForm(null,'target_path', 'filter') }}" placeholder="Target Path" class="filter-grid" />
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <input type="text" name="filter[params]" value="{{ Config::getValueForm(null,'params', 'filter') }}" placeholder="Params" class="filter-grid" />
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <input type="text" name="filter[redirect]" value="{{ Config::getValueForm(null,'redirect', 'filter') }}" placeholder="Redirect" class="filter-grid" />
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @if($model && count($model))
                                @foreach($model as $item)
                                    <tr>
                                        <td>
                                            <input type="checkbox" class="input-checkbox item-id mass-item" name="item[]" value="{{ $item->id }}"
                                        </td>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->type }}</td>
                                        <td>{{ $item->request_path }}</td>
                                        <td>{{ $item->target_path }}</td>
                                        <td>{{ $item->params }}</td>
                                        <td>{{ $item->redirect }}</td>
                                        <td>
                                            <a href="{{ url(config('base.admin_uri').'/url/rewrite/delete/'.$item->id.'/'.csrf_token()) }}" class="button btn-delete delete-confirm">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
