<?php
if(!$model->count()) {
    return;
}
?>
<div class="row">
    <div class="col-sm-12">
        <div class="row">
            <div class="limit pull-left col-sm-3">
                <label>Show</label>
                <select name="limit" class="form-control toolbar-input input-select" onchange="window.location.href = this.value;">
                    @foreach(\App\Helpers\Option::limit() as $option)
                        <option value="{{ \App\Helpers\Config::urlReplaceParams(['limit' => $option['value']]) }}"
                            @if ($option['value'] == $model->perPage())
                                    selected
                                @endif
                        >{{ $option['label'] }}</option>
                    @endforeach
                </select>
            </div>
            @if($model->lastPage() > 1)
                <div class="pager col-sm-8 pull-right">
                    <ul class="pagination">
                        <li class="paginate_button
                            @if($model->currentPage() == 1)
                                disabled
                            @endif
                        ">
                            <a href="
                                @if($model->currentPage() != 1)
                                    {{ \App\Helpers\Config::urlReplaceParams(['page' => 1]) }}
                                @else
                                    #
                                @endif
                            ">
                                <i class="fa fa-angle-double-left"></i>
                            </a>
                        </li>
                        <li class="paginate_button
                            @if($model->currentPage() == 1)
                                disabled
                            @endif
                        ">
                            <a href="
                               @if($model->currentPage() != 1)
                                    {{ \App\Helpers\Config::urlReplaceParams(['page' => $model->currentPage()-1]) }}
                                @else
                                    #
                                @endif
                            ">
                                <i class="fa fa-arrow-left"></i>
                            </a>
                        </li>
                        <li class="paginate_button active">
                            <a href="{{ \App\Helpers\Config::urlReplaceParams(['page' => $model->currentPage()]) }}">
                                {{ $model->currentPage() }}
                            </a>
                        </li>
                        <?php
                        //page next util step
                        $nextUtilPage = ($model->currentPage() + 3) < $model->lastPage() ?
                                ($model->currentPage() + 3) : ($model->lastPage());
                        ?>
                        @for ($i = ($model->currentPage() + 1) ; $i <= $nextUtilPage ; $i++)
                        <li class="paginate_button<?php 
                            if($model->currentPage() == $i): ?> active<?php endif; ?>">
                            <a href="{{ \App\Helpers\Config::urlReplaceParams(['page' => $i]) }}">
                                {{ $i }}
                            </a>
                        </li>
                        @endfor
                        <li class="paginate_button
                            @if(!$model->hasMorePages())
                                disabled
                            @endif
                        ">
                            <a href="
                               @if($model->hasMorePages())
                                    {{ \App\Helpers\Config::urlReplaceParams(['page' => $model->currentPage()+1]) }}
                                @else
                                    #
                                @endif
                            ">
                                <i class="fa fa-arrow-right"></i>
                            </a>
                        </li>
                        <li class="paginate_button
                            @if($model->lastPage() == $model->currentPage())
                                disabled
                            @endif
                        ">
                            <a href="
                               @if($model->lastPage() != $model->currentPage())
                                    {{ \App\Helpers\Config::urlReplaceParams(['page' => $model->lastPage()]) }}
                                @else
                                    #
                                @endif
                            ">
                                <i class="fa fa-angle-double-right"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            @endif
        </div>
    </div>
</div>

