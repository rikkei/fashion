@extends('layouts.default')

@section('content')
<?php use App\Model\Product; ?>

<div class="product-wrapper">
    <div class="row">
        <div class="col-sm-6 product-image-box">
            @include('product.view.media')
        </div>
        <div class="col-sm-6 product-shop">
            <form action="{{ url('cart/add') }}" method="post" class="form-add-cart">
                {!! csrf_field() !!}
                <input type="hidden" name="id" value="{{ $product->id }}" />
                <input type="hidden" name="_back" value="{{ Request::url() }}" />
                
                <h2 class="product-name">{{ $product->name }}</h2>
                <p class="product-sku">
                    <label>Sku:</label>
                    <span>{{ $product->sku }}</span>
                </p>
                @if($product->short_description)
                    <div class="product-short-description">
                        {!! $product->short_description !!}
                    </div>
                @endif
                {!! \App\Helpers\Output::priceProduct($product) !!}

                <div class="options-cart">
                    <div class="form-group row">
                        <label for="qty" class="col-sm-2">Qty</label>
                        <div class="col-sm-9">
                            <input type="number" name="qty" class="form-control qty" 
                                   id="qty" placeholder="qty" value="1" />
                        </div>
                    </div>
                </div>
                <div class="actions">
                    <button type="submit" class="btn btn-info btn-add-cart">
                        <span>Add To Cart</span>
                    </button>
                </div>
            </form>
        </div>
    </div>
    
    <div class="row product-more-info">
        <div class="col-sm-12">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#product-description">Description</a></li>
            </ul>
            <div class="tab-content">
                <!-- Tab General Category -->
                <div id="product-description" class="tab-pane active">
                    @if($product->description)
                        {!! $product->description !!}
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
