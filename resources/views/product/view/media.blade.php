<?php
//default value
$column = 4;
$count = count($gallery);
$i = 0;
?>

@if(!$gallery || !$count)
    <div class="image-base no-image">
        <img src="{{ url(config('base.product_image')) }}" />
    </div>
@else
    <div id="carousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            @foreach($gallery as $item)
                @if($i == 0)
                    <div class="item active">
                @else
                    <div class="item">
                @endif
                    <img src="{{ url($item->path) }}" />
                </div>
                <?php $i++; ?>
            @endforeach
        </div>
    </div>
    <div class="clearfix">
        <div id="thumbcarousel" class="carousel slide" data-interval="false">
            <div class="carousel-inner">
                <?php $i = 0; ?>
                @foreach ($gallery as $item)
                    @if ($i == 0)
                        <div class="item active">
                    @elseif ($i % $column == 0)
                        <div class="item">
                    @endif                
                    <div data-target="#carousel" data-slide-to="{{ $i }}" class="thumb">
                        <img src="{{ url($item->path) }}">
                    </div>
                    
                    @if ($i != 0 && ($i % 4 == 3 || $i == ($count - 1)))
                        </div>
                    @endif
                    <?php $i++; ?>
                @endforeach
            </div>
            
            <a class="left carousel-control" href="#thumbcarousel" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <a class="right carousel-control" href="#thumbcarousel" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
        </div>
    </div>
@endif
