@extends('layouts.default')

@section('content')
<?php use \App\Helpers\Output; ?>
<div class="cart-page">
    <h1 class="page-title">My Cart</h1>
    @if(isset($items) && count($items))
        <div class="row cart-info">
            <div class="col-sm-12">
                <table class="table table-responsive">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Unit</th>
                            <th>Qty</th>
                            <th>Subtotal</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($items as $id => $item)
                        <tr>
                            <td>{{ $item['name'] }}</td>
                            <td>{!! Output::price($item['price']) !!}</td>
                            <td>{{ $item['qty'] }}</td>
                            <td>{!! Output::price($item['qty'] * $item['price']) !!}</td>
                            <td>
                                <a class="btn btn-delete" href="{{ url('cart/delete/'.$id.'/'.csrf_token()) }}">Delete</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <form class="form-checkout" action="{{ url('cart/checkout') }}" method="post">
            {!! csrf_field() !!}

            <div class="cart-total-box row">
                <div class="col-sm-12">
                    <div class="cart-total">
                        <p>
                            <label>Total:</label>
                            <span>{!! Output::price($totalPrice) !!}</span>
                        </p>
                    </div>
                    <div class="cart-actions">
                        <button type="submit" class="btn btn-info">
                            <span>Checkout</span>
                        </button>
                    </div>
                </div>
            </div>

            <div class="order-info">
                <div class="row">
                    <div class="form-group col-sm-6">
                        <label for="name">Name</label>
                        <input type="text" name="order[user_name]" class="form-control" 
                               id="name" placeholder="Name" value="{{ old('order.user_name') }}" />
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="email">Email</label>
                        <input type="email" name="order[user_email]" class="form-control" 
                                   id="email" placeholder="Email" value="{{ old('order.user_email') }}" />
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="phone">Phone</label>
                        <input type="text" name="order[user_phone]" class="form-control" 
                                   id="phone" placeholder="Phone" value="{{ old('order.user_phone') }}" />
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group col-sm-6">
                        <label for="address">Address</label>
                        <textarea name="order[user_address]" class="form-control" 
                            id="address" placeholder="Address">{!! nl2br(strip_tags(old('order.user_address'))) !!}</textarea>
                    </div>
                </div>
            </div>

            <div class="cart-actions">
                <button type="submit" class="btn btn-info">
                    <span>Checkout</span>
                </button>
            </div>
        </form>


    @else
        <p class="note empty">Not any item in Cart</p>
    @endif
</div>
@endsection
