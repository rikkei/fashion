@extends('layouts.default')

@section('content')

<h2 class="page-title">Order Success</h2>
<div class="order-sucess-info">
    <p>
        <label>Id:</label>
        <span>{{ $order->id }}</span>
    </p>
    <p>
        <label>Name:</label>
        <span>{{ $order->user_name }}</span>
    </p>
    <p>
        <label>Email:</label>
        <span>{{ $order->user_email }}</span>
    </p>
    <p>
        <label>Phone:</label>
        <span>{{ $order->user_phone }}</span>
    </p>
    <p>
        <label>Address:</label>
        <span>{!! nl2br($order->user_address) !!}</span>
    </p>
    <p>
        <label>Amount:</label>
        <span>{!! \App\Helpers\Output::price($order->grandtotal) !!}</span>
    </p>
</div>

@endsection
