@extends('layouts.default')

@section('content')
<?php use App\Model\News; ?>
<div class="toolbar">
    @include('include.pager')
</div>
<div class="news-wrapper">
    @if($model && count($model))
        <ul class="news-list row list-format">
            @foreach($model as $item)
                <li class="item col-sm-12">
                    <div class="row">
                        @if(News::getThumbnail($item))
                            <div class="news-image col-sm-4">
                                <a href="{{ News::getSlug($item) }}">
                                    <img src="{{ News::getThumbnail($item) }}" 
                                         alt="{{ $item->name }}" 
                                        title="{{ $item->name }}" />
                                </a>
                            </div>
                        @endif
                        <div class="product-shop
                             @if(News::getThumbnail($item))
                                col-sm-8
                            @else
                                col-sm-12
                            @endif
                            ">
                            <h2 class="news-name">
                                <a href="{{ News::getSlug($item) }}">
                                    {{ $item->name }}
                                </a>
                            </h2>
                            <div class="news-meta">
                                <p><i class="fa fa-clock-o"></i> 
                                    {{ \App\Helpers\Output::date($item->created_at) }}</p>
                            </div>
                            <div class="news-short-description">
                                {!! $item->short_description !!}
                            </div>
                        </div>
                    </div>
                </li>
            @endforeach
        </ul>
    @else
    <p class="note empty">Not found any news</p>
    @endif
</div>
@endsection
