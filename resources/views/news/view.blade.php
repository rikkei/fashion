@extends('layouts.default')

@section('content')
<?php use App\Model\News; ?>
<div class="news-view-wrapper">
    @if(News::getImage($model))
        <div class="news-image">
            <a href="{{ News::getSlug($model) }}">
                <img src="{{ News::getImage($model) }}" 
                     alt="{{ $model->name }}" 
                    title="{{ $model->name }}" />
            </a>
        </div>
    @endif
    <div class="news-info">
        <h1>{{ $model->name }}</h1>
        <div class="news-meta">
            <p><i class="fa fa-clock-o"></i> {{ $model->created_at }}</p>
        </div>
        <div class="news-description">
            {!! $model->description !!}
        </div>
    </div>
</div>
@endsection
