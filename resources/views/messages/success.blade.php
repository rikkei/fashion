<?php
if(!Session::has('messages')) {
    return;
}
$messages = Session::get('messages');
app('session')->forget('messages');
?>

@if (isset($messages['success']) && count($messages['success']))
    <div class="row">
        <div class="col-sm-12">
            <div class="flash-message">
                <div class="alert alert-success">
                    <ul>
                        @foreach($messages['success'] as $message)
                            <li>{{ $message }}</li>   
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endif
