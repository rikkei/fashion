@extends('layouts.default')

@section('content')
<?php 
use App\Model\Product; 

//value default;
$column = 4;
?>

@if($category->image)
    <div class="row">
        <div class="col-sm-12 category-image">
            <img src="{{ $category->image }}" alt="{{ $category->name }}" title="{{ $category->name }}" />
        </div>
        @if($category->description)
        <div class="col-sm-12 category-description">
            {!! $category->description !!}
        </div>
        @endif
    </div>
@endif

<div class="toolbar">
    @include('category.list.toolbar')
</div>
<div class="product-list-wrapper">
    @if($model && count($model))
        <ul class="products-grid">
            @foreach($model as $item)
                <?php $item = Product::loadItem($item, ['name', 'price', 'special_price']); ?>
                <li class="item col-sm-3">
                    <div class="product-image">
                        <a href="{{ Product::getSlug($item) }}">
                            <img src="{{ Product::getImageUrl($item->id) }}" 
                                 alt="{{ $item->name }}" 
                                title="{{ $item->name }}" />
                        </a>
                    </div>
                    <div class="product-shop">
                        <h2 class="product-name">
                            <a href="{{ Product::getSlug($item) }}">
                                {{ $item->name }}
                            </a>
                        </h2>
                        {!! \App\Helpers\Output::priceProduct($item) !!}
                    </div>
                </li>
            @endforeach
        </ul>
    @else
    <p class="note empty">Not found any product!</p>
    @endif
</div>
@endsection
