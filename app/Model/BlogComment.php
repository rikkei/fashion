<?php
namespace App\Model;

use DB;
use URL;

class BlogComment extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'blog_comment';
    protected $fillable = array('user_id', 'blog_id', 'content', 
        'parent_id', 'approve');
    
    /**
     * get collection to show grid
     * 
     * @return type
     */
    public static function getAllGrid()
    {
        $pager = \App\Helpers\Config::getPagerOption();
        $order = app('request')->input('order');
        $dir = app('request')->input('dir');
        if ($order) {
            if ($dir != 'asc') {
                $dir = 'desc';
            }
            $model = self::orderBy($order, $dir);
        } else {
            $model = self::orderBy('id');
        }
        $model = $model->select('id', 'user_id', 'blog_id', 'parent_id', 'approve');
        $request = app('request')->input('filter');
        if ($request && count($request)) {
            foreach ($request as $key => $value) {
                if (in_array($key, self::$filterModel)) { echo 'a';exit;
                    if (is_array($value)) {
                        if (isset($value['from']) && $value['from']) {
                            $model = $model->where($key,'>=',$value['from']);
                        }
                        if (isset($value['to']) && $value['to']) {
                            $model = $model->where($key,'<=',$value['to']);
                        }
                        continue;
                    }
                }
                $model = $model->where($key,'like',"%$value%");
            }
        }
        $model = $model->paginate($pager['limit']);
        return $model;
    }
    
    /**
     * get list new frontend
     * 
     * @param type $urlRewrite
     * @return type
     */
    public static function getList($urlRewrite = true)
    {
        $pager = \App\Helpers\Config::getPagerOption();
        $model = self::select('news.id as id', 'thumbnail', 'name', 
                'short_description', 'news.created_at as created_at')
            ->where('active', '=', 1)
            ->orderBy('news.created_at', 'DESC')
            ->groupBy('news.id');
        if ($urlRewrite) {
            $model = $model->addSelect('request_path as url')
                ->leftJoin('url_rewrite', function($join) {
                    global $categoryIdCalBack;
                    $join->on('url_rewrite.item_id', '=', 'news.id')
                        ->where('type', '=', 'news');
                });
        }
        $model = $model->paginate($pager['limit']);
        return $model;
    }

    /**
     * get url of news
     *  find in table url rewrite or default news url
     *
     * @param $id
     * @return string
     */
    public static function getUrl($id)
    {
        $itemRewrite = DB::table('url_rewrite')
            ->where('type', '=', 'news')
            ->where('item_id', '=', $id)
            ->orderBy('id', 'desc');
        $itemRewrite = $itemRewrite->first();
        if (count($itemRewrite)) {
            return url($itemRewrite->request_path);
        }
        return url('news/'.$id);
    }

    /**
     * get slug url
     *
     * @param $model
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    public static function getSlug($model){
        if( isset($model->url) && $model->url) {
            return url($model->url);
        }
        return url('news/'.$model->id);
    }
    
    /**
     * get thumbnail of news
     * 
     * @param type $model
     * @return string|null
     */
    public static function getThumbnail($model)
    {
        if( isset($model->thumbnail) && $model->thumbnail) {
            return URL::asset($model->thumbnail);
        }
        return null;
    }
    
    /**
     * get image of news
     * 
     * @param type $model
     * @return string|null
     */
    public static function getImage($model)
    {
        if( isset($model->image) && $model->image) {
            return URL::asset($model->image);
        }
        return null;
    }
}

