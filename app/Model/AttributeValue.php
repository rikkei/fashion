<?php
namespace App\Model;

use DB;

class AttributeValue extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'attribute_value';
    protected $fillable = array('attribute_id', 'product_id', 'value');
    
    /**
     * create new value of product
     * 
     * @param type $request
     * @param type $productId
     */
    public static function createNew($request, $productId)
    {
        foreach ($request as $attributeId => $value) {
            if ($value !== null) {
                if (is_array($value)) {
                    $value = implode(',', $value);
                }
                self::create([
                    'attribute_id' => $attributeId,
                    'product_id' => $productId,
                    'value' => $value,
                ]);
            }
        }
    }
    
    /**
     * Update attrbite value
     * 
     * @param type $request
     * @param type $productId
     */
    public static function updateItem($request, $productId)
    {
        foreach ($request as $attributeId => $value) {
            if ($value !== null) {
                if (is_array($value)) {
                    $value = implode(',', $value);
                }
                self::where('attribute_id','=',$attributeId)
                    ->where('product_id','=',$productId)
                    ->update([
                    'value' => $value,
                ]);
            }
        }
    }
    
    /**
     * Save Attribute value:
     *  Update if exists
     *  Insert if not exists
     * 
     * @param type $request
     * @param type $productId
     */
    public static function saveItem($request, $productId)
    {
        foreach ($request as $attributeId => $value) {
            if ($value !== null) {
                if (is_array($value)) {
                    $value = implode(',', $value);
                }
                $attributeValue = DB::table('attribute_value')
                    ->select('id')
                    ->where('attribute_id','=',$attributeId)
                    ->where('product_id','=',$productId)
                    ->first();
                //update
                if (count($attributeValue)) {
                    self::find($attributeValue->id)
                        ->update([
                        'value' => $value,
                    ]);
                } else { //insert
                    self::create([
                        'attribute_id' => $attributeId,
                        'product_id' => $productId,
                        'value' => $value,
                    ]);
                }
            }
        }
    }
}
