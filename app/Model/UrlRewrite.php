<?php
namespace App\Model;

use DB;

class UrlRewrite extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'url_rewrite';
    protected $fillable = array('type', 'request_path', 'target_path',
        'params', 'redirect', 'item_id');

    //store path url of category and product
    private static $indexUrlCategoryPath = array();
    private static $indexUrlProductPath = array();
    /**
     * get collection to show grid
     *
     * @return type
     */
    public static function getAllGrid()
    {
        $pager = \App\Helpers\Config::getPagerOption();
        $model = self::select('id','type','request_path','target_path','redirect', 'params')
            ->orderBy('id');
        $request = app('request')->input('filter');
        if ($request && count($request)) {
            foreach ($request as $key => $value) {
                if (is_array($value)) {
                    if (isset($value['from']) && $value['from']) {
                        $model = $model->where($key,'>=',$value['from']);
                    }
                    if (isset($value['to']) && $value['to']) {
                        $model = $model->where($key,'<=',$value['to']);
                    }
                    continue;
                }
                $model = $model->where($key,'like',"%$value%");
            }
        }
        $model = $model->paginate($pager['limit']);
        return $model;
    }

    /**
     * load url rewrite item by request path
     *
     * @param $path
     * @return model
     */
    public static function loadByRequestPath($path)
    {
        return self::where('request_path', '=', $path)->first();
    }

    /**
     * reindex url rewrite
     *  - category
     *  - product
     */
    public static function reindex()
    {
        self::$indexUrlCategoryPath = array();
        self::$indexUrlProductPath = array();
        self::indexCategory();
        self::indexProduct();
        self::indexNews();
    }

    /**
     * reindex category
     */
    public static function indexCategory()
    {
        $category = DB::table('category')
            ->select('id', 'url_key', 'parent_id')
            ->get();
        if (count($category)) {
            foreach ($category as $item) {
                self::$indexUrlCategoryPath[$item->id] = [
                    'parent_id' => $item->parent_id,
                    'url_key' => $item->url_key,
                ];
            }
        }
        self::processUrlPathCategory(self::$indexUrlCategoryPath);
        foreach (self::$indexUrlCategoryPath as $id => $valueUrlKey) {
            $urlKey  = trim($valueUrlKey['url_key'],'/');
            $itemRewrite = DB::table('url_rewrite')
                ->select('id', 'request_path')
                ->where('type', '=', 'category')
                ->where('item_id' , '=', $id)
                ->first();
            if (count($itemRewrite)) {
                $itemRequestPath = DB::table('url_rewrite')
                    ->select('id')
                    ->where('request_path', '=', $urlKey)
                    ->where('id', '<>', $itemRewrite->id)
                    ->first();
                if (count($itemRequestPath)) {
                    $urlKey .= '-c'.$id;
                }
                if($itemRewrite->request_path != $urlKey) {
                    DB::table('url_rewrite')
                        ->where('id', '=', $itemRewrite->id)
                        ->update([
                            'request_path' => $urlKey,
                        ]);
                }
            }
            else {
                $itemRequestPath = DB::table('url_rewrite')
                    ->select('id')
                    ->where('request_path', '=', $urlKey)
                    ->first();
                if (count($itemRequestPath)) {
                    $urlKey .= '-c'.$id;
                }
                DB::table('url_rewrite')
                    ->insert([
                        'type' => 'category',
                        'request_path' => $urlKey,
                        'target_path' => 'category/'.$id,
                        'item_id' => $id,
                        'redirect' => 0,
                    ]);
            }
        }
    }

    /**
     * process url path category
     */
    protected static function processUrlPathCategory()
    {
        if(count(self::$indexUrlCategoryPath)) {
            foreach (self::$indexUrlCategoryPath as $id => $value) {
                self::processUrlPathCategoryRecursive($id);
            }
        }
    }

    /**
     * process path url category recursive
     *
     * @param $id
     * @return string
     */
    protected static function processUrlPathCategoryRecursive($id)
    {
        if(!isset(self::$indexUrlCategoryPath[$id]) ||
            !isset(self::$indexUrlCategoryPath[$id]['url_key'])) {
            return '';
        }
        $url = self::$indexUrlCategoryPath[$id]['url_key'];
        if(isset(self::$indexUrlCategoryPath[$id]['parent_id']) &&
            self::$indexUrlCategoryPath[$id]['parent_id'] != 0) {
            $url = self::processUrlPathCategoryRecursive(self::$indexUrlCategoryPath[$id]['parent_id']) . '/' . $url;
        }
        unset(self::$indexUrlCategoryPath[$id]['parent_id']);
        self::$indexUrlCategoryPath[$id]['url_key'] = $url;
        return $url;
    }

    /**
     * reindex product
     */
    public static function indexProduct()
    {
        $product = DB::table('product')
            ->select('product.id as id', 'category_product.category_id as category_id',
                'attribute_value.value as url_key')
            ->leftjoin('category_product', 'category_product.product_id', '=', 'product.id')
            ->join('attribute_value', 'attribute_value.product_id', '=', 'product.id')
            ->join('attribute', 'attribute.id', '=', 'attribute_value.attribute_id')
            ->where('attribute.code', '=', 'url_key')
            ->orderBy('product.id')
            ->get();

        if (count($product)) {
            foreach ($product as $item) {
                self::$indexUrlProductPath[$item->id . '-' . $item->category_id] = [
                    'id' => $item->id,
                    'category_id' => $item->category_id,
                    'url_key' => $item->url_key,
                ];
            }
        }
        self::processUrlPathProduct();
        foreach (self::$indexUrlProductPath as $idPc => $valueUrlKey) {
            if ($valueUrlKey['category_id']) {
                $params = 'category/'.$valueUrlKey['category_id'];
            } else {
                $params = '';
            }
            $urlKey  = trim($valueUrlKey['url_key'],'/');
            $itemRewrite = DB::table('url_rewrite')
                ->select('id', 'request_path')
                ->where('item_id', '=', $valueUrlKey['id'])
                ->where('params' , '=', $params)
                ->where('type', '=', 'product')
                ->first();
            if (count($itemRewrite)) {
                $itemRequestPath = DB::table('url_rewrite')
                    ->select('id')
                    ->where('request_path', '=', $urlKey)
                    ->where('id', '<>', $itemRewrite->id)
                    ->first();
                if (count($itemRequestPath)) {
                    $urlKey .= '-p'.$idPc;
                }
                if ($itemRewrite->request_path != $urlKey) {
                    DB::table('url_rewrite')
                        ->where('id', '=', $itemRewrite->id)
                        ->update([
                            'request_path' => $urlKey,
                        ]);
                }
            }
            else {
                $itemRequestPath = DB::table('url_rewrite')
                    ->select('id')
                    ->where('request_path', '=', $urlKey)
                    ->first();
                if (count($itemRequestPath)) {
                    $urlKey .= '-p'.$idPc;
                }
                DB::table('url_rewrite')
                    ->insert([
                        'type' => 'product',
                        'request_path' => $urlKey,
                        'target_path' => 'product/'.$valueUrlKey['id'],
                        'item_id' => $valueUrlKey['id'],
                        'params' => $params,
                        'redirect' => 0,
                    ]);
            }
        }
    }


    /**
     * process url path Product
     */
    protected static function processUrlPathProduct()
    {
        if(count(self::$indexUrlProductPath)) {
            foreach (self::$indexUrlProductPath as $idPC => $value) {
                self::processUrlPathProductItem($idPC);
            }
        }
    }

    /**
     * process path url each item product
     *
     * @param $id
     */
    protected static function processUrlPathProductItem($idPC)
    {
        if(!isset(self::$indexUrlProductPath[$idPC]) ||
            !isset(self::$indexUrlProductPath[$idPC]['url_key']))
        {
            return '';
        }
        $url = self::$indexUrlProductPath[$idPC]['url_key'];
        if (isset(self::$indexUrlProductPath[$idPC]['category_id']) &&
            $idCategory = self::$indexUrlProductPath[$idPC]['category_id'])
        {
            if (isset(self::$indexUrlCategoryPath[$idCategory])) {
                $url = self::$indexUrlCategoryPath[$idCategory]['url_key'] . '/' . $url;
            }
        }
        self::$indexUrlProductPath[$idPC]['url_key'] = $url;
    }
    
    /**
     * reindex news
     */
    public static function indexNews()
    {
        $collection = DB::table('news')
            ->select('id', 'url_key')
            ->get();
        if (!count($collection)) {
            return;
        }
        foreach ($collection as $item) {
            $urlKey = trim($item->url_key, '/');
            $id = $item->id;
            $itemRewrite = DB::table('url_rewrite')
                ->select('id', 'request_path')
                ->where('type', '=', 'news')
                ->where('item_id' , '=', $id)
                ->first();
            if (count($itemRewrite)) {
                $itemRequestPath = DB::table('url_rewrite')
                    ->select('id')
                    ->where('request_path', '=', $urlKey)
                    ->where('id', '<>', $itemRewrite->id)
                    ->first();
                if (count($itemRequestPath)) {
                    $urlKey .= '-n'.$id;
                }
                if($itemRewrite->request_path != $urlKey) {
                    DB::table('url_rewrite')
                        ->where('id', '=', $itemRewrite->id)
                        ->update([
                            'request_path' => $urlKey,
                        ]);
                }
            }
            else {
                $itemRequestPath = DB::table('url_rewrite')
                    ->select('id')
                    ->where('request_path', '=', $urlKey)
                    ->first();
                if (count($itemRequestPath)) {
                    $urlKey .= '-n'.$id;
                }
                DB::table('url_rewrite')
                    ->insert([
                        'type' => 'news',
                        'request_path' => $urlKey,
                        'target_path' => 'news/'.$id,
                        'item_id' => $id,
                        'redirect' => 0,
                    ]);
            }
        }
    }
}
