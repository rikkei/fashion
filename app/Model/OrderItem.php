<?php
namespace App\Model;

use DB;

class OrderItem extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'order_item';
    protected $fillable = array('order_id', 'product_id', 'name', 'qty', 
        'price', 'sku');
    
}
