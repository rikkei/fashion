<?php
namespace App\Model;

class Attribute extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'attribute';
    protected $fillable = array('code', 'label', 'required', 'unique');
    
    /**
     * get active record array
     * 
     * @return type
     */
    public static function getAttributeEdit()
    {
        $attributeCode = ['name','url_key','price','special_price',
            'special_price_from','special_price_to','short_description','description',
            'qty','stock_status'];
        $model = self::select('id','code','label','required','unique')
                ->whereIn('code',$attributeCode)->get();
        $model = \App\Helpers\Model::toArrayCodeKey($model);
        return $model;
    }
    
    /**
     * 
     * check data input validate + url_key get from name if it empty
     * 
     * @param type $request
     * @param type $productId
     * @return boolean|string
     */
    public static function checkDataInput(&$request, $productId = null)
    {
        if (!$request || !count($request)) {
            return false;
        }
        $errors = array();
        $name = null;
        foreach ($request as $id => $value) {
            $attribute = Attribute::find($id);
            if (count($attribute)) {
                if ($attribute->code == 'name') {
                    $name = $value;
                    $name = \App\Helpers\Config::urlSlug($name);
                }
                if ($attribute->code == 'url_key' && !$value) {
                    $request[$id] = $name;
                    $value = $name;
                }
                if ($attribute->required && !$value) {
                    $errors[] = $attribute->label . ' is required.';
                }
                if ($attribute->unique && $value) {
                    $attributeAnother = AttributeValue::select('attribute_value.id')
                            ->join('attribute','attribute_value.attribute_id','=','attribute.id')
                            ->where('attribute_id','<>',$productId)
                            ->where('value','=',$value)
                            ->get();
                    if (count($attributeAnother)) {
                        $errors[] = $attribute->label . ' is unique.';
                    }
                }
            }
        }
        return $errors;
    }
}

