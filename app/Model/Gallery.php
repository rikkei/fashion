<?php
namespace App\Model;

use DB;

class Gallery extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'gallery';
    protected $fillable = array('path', 'alt', 'title', 'position',     'disable', 'product_id');
    
    /**
     * create new gallery
     * 
     * @param type $productId
     * @param type $galleryData
     */
    public static function createNewGallery($productId, $galleryData)
    {
        foreach ($galleryData as $gallery) {
            if (isset($gallery['delete']) && $gallery['delete']) {
                continue;
            }
            if (!$gallery['path']) {
                continue;
            }
            $gallery = array_merge($gallery, array('product_id' => $productId));
            self::create($gallery);
        }
    }
    
    public static function saveGallery($galleryData)
    {
        if(!$galleryData || !count($galleryData)) {
            return;
        }
        foreach ($galleryData as $galleryId => $gallery) {
            if (isset($gallery['delete']) && $gallery['delete']) {
                DB::table('gallery')->where('id', '=', $galleryId)->delete();
                continue;
            }
            if (!$gallery['path']) {
                continue;
            }
            if(!isset($gallery['disable']) || !$gallery['disable']) {
                $gallery['disable'] = 0;
            }
            DB::table('gallery')->where('id', '=', $galleryId)->update($gallery);
        }
    }
    
    
    public static function getGalleryProduct($productId, $disable = false)
    {
        $model = self::where('product_id', '=', $productId)
                ->orderBy('position');
        if(!$disable) {
            $model = $model->where('disable', '<>', '1');
        }
        return $model->get();
    }
}
