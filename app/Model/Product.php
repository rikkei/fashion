<?php
namespace App\Model;

use DB;

class Product extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'product';
    protected $fillable = array('active', 'sku');
    protected static $filterModel = array('id', 'active', 'sku');
    
    /**
     * get collection to show grid
     * 
     * @return type
     */
    public static function getAllGrid()
    {
        $pager = \App\Helpers\Config::getPagerOption();
        $model = self::select('product.id as id')->orderBy('id');
        $request = app('request')->input('filter');
        if (!$request || !count($request)) {
            $model = $model->paginate($pager['limit']);
            return $model;
        }
        
        $flagCheckJoin = false;
        foreach ($request as $key => $value) {
            global $valueGlobal, $keyGlobal;
            $valueGlobal = $value;
            $keyGlobal = $key;

            if (in_array($key, self::$filterModel)) {
                if (is_array($value)) {
                    if (isset($value['from']) && $value['from']) {
                        $model = $model->where($key,'>=',$value['from']);
                    }
                    if (isset($value['to']) && $value['to']) {
                        $model = $model->where($key,'<=',$value['to']);
                    }
                    continue;
                } else {
                    $model = $model->where($key,'like',"%$value%");
                }
            } else {
                if (!$flagCheckJoin) {
                    $model = $model
                    ->join('attribute_value', 'attribute_value.product_id', '=', 'product.id')
                    ->join('attribute', 'attribute_value.attribute_id', '=', 'attribute.id');
                    $flagCheckJoin = true;
                }
                $model = $model
                    ->orWhere(function($query) {
                        global $valueGlobal, $keyGlobal;
                        $query = $query->where('attribute.code', '=', $keyGlobal);
                        if (is_array($valueGlobal)) {
                            if (isset($valueGlobal['from']) && $valueGlobal['from']) {
                                $query->where('attribute_value.value', '>=', $valueGlobal['from']);
                            }
                            if (isset($valueGlobal['to']) && $valueGlobal['to']) {
                                $query->where('attribute_value.value', '<=', $valueGlobal['to']);
                            }
                        } else {
                            $query->where('attribute_value.value', 'like', "%$valueGlobal%");
                        }
                    });
            }
        }
        $model = $model->groupBy('product.id');
        $model = $model->paginate($pager['limit']);
        return $model;
    }
    
    /**
     * Load collection product with atrtibute
     * 
     * @param string $attributes
     * @return Product model
     */
    public static function loadCollection($attributes = array())
    {
        if (!$attributes || !count($attributes)) {
            $attributes = [
                'name',
                'url_key',
                'price',
                'qty',
                'stock_status',
            ];
        }
        $select = [
            'product.id as id',
            'active',
            'sku',
            'code',
            'label',
            'attribute_value.id as vaule_id',
            'value',
        ];
        $model = DB::table('product')
                ->select($select)
                ->join('attribute_value', 'attribute_value.product_id', '=', 'product.id')
                ->join('attribute', 'attribute.id', '=', 'attribute_value.attribute_id')
                ->whereIn('code',$attributes)
                ->orderBy('id')
                ->get();
        return $model;
    }
    
    /**
     * 
     * 
     * @param type $id
     * @param type $attributes
     * @return boolean|product model
     */
    public static function loadItem($model, $attributes = array()) {
        if(!is_object($model)) {
            $model = self::find($model);
            if (!count($model)) {
                return false;
            }
        }
        $id = $model->id;
        $attributeValue = DB::table('attribute_value')
                ->select('code','value')
                ->join('attribute','attribute_value.attribute_id','=','attribute.id')
                ->where('product_id','=',$id);
        if ($attributes && count($attributes)) {
            $attributeValue = $attributeValue->whereIn('code',$attributes);
        }
        $attributeValue = $attributeValue->get();
        foreach ($attributeValue as $item) {
            $model->{$item->code} = $item->value;
        }
        if (isset($model->special_price) && $model->special_price) {
            $model->final_price = $model->special_price;
        } else {
            $model->final_price = $model-> price;
        }
        return $model;
    }
    
    /**
     * override delete model - delete all child of category
     */
    public function delete() {
        $id = $this->id;
        parent::delete();
        DB::table('attribute_value')
                ->where('product_id','=',$id)
                ->delete();
    }
    
    /**
     * get collection to show grid
     * 
     * @return type
     */
    public static function getProductCategory($categoryId, $urlRewrite = true)
    {
        $pager = \App\Helpers\Config::getPagerOption();
        global $categoryIdCalBack;
        $categoryIdCalBack = $categoryId;
        $model = DB::table('product')
            ->select('product.id as id')
            ->join('category_product', 'category_product.product_id', '=', 'product.id');
        if ($urlRewrite) {
            $model = $model->addSelect('request_path as url')
                ->leftJoin('url_rewrite', function($join) {
                    global $categoryIdCalBack;
                    $join->on('url_rewrite.item_id', '=', 'product.id')
                        ->where('type', '=', 'product')
                        ->where('params', '=', 'category/'.$categoryIdCalBack);
                });
        }
        $model = $model->where('category_product.category_id', '=', $categoryId)
            ->where('product.active', '=', 1)
            ->orderBy('category_product.position')
            ->orderBy('product.id', 'DESC')
            ->groupBy('product.id');

        $model = $model->paginate($pager['limit']);
        return $model;
    }
    
    /**
     * get image url in gallery
     * 
     * @param type $id
     * @return url imaga
     */
    public static function getImageUrl($id)
    {
        if (is_object($id)) {
            $id = $id->id;
        }
        $image = DB::table('gallery')->where('product_id', '=', $id)
            ->where('disable', '=', '0')
            ->orderBy('id', 'ASC')
            ->first();
        if($image && count($image)) {
            return url($image->path);
        }
        return url(config('base.product_image'));
    }

    /**
     * get url of product
     *  find in table url rewrite or default product url
     *
     * @param $id
     * @return string
     */
    public static function getUrl($id, $categoryId = null)
    {
        $itemRewrite = DB::table('url_rewrite')
            ->where('type', '=', 'product')
            ->where('item_id', '=', $id)
            ->orderBy('id', 'desc');
        if ($categoryId) {
            $itemRewrite = $itemRewrite->where('params', '=', 'category/'.$categoryId);
        }
        $itemRewrite = $itemRewrite->first();
        if (count($itemRewrite)) {
            return url($itemRewrite->request_path);
        }
        return url('product/'.$id);
    }

    /**
     * get slug url
     *
     * @param $model
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    public static function getSlug($model){
        if( isset($model->url) && $model->url) {
            return url($model->url);
        }
        return url('product/'.$model->id);
    }
}

