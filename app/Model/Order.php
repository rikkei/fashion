<?php
namespace App\Model;

use DB;

class Order extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'order';
    protected $fillable = array('status', 'user_id', 'user_name', 'user_email', 
        'user_phone', 'user_address', 'subtotal', 'grandtotal');
    
    protected $status = array(
        'new' => 'New',
        'process' => 'Process',
        'complete' => 'Complete',
    );
    
    /**
     * get collection to show grid
     * 
     * @return collection model
     */
    public static function getAllGrid()
    {
        $pager = \App\Helpers\Config::getPagerOption();
        $order = app('request')->input('order');
        $dir = app('request')->input('dir');
        if ($order) {
            if ($dir != 'asc') {
                $dir = 'desc';
            }
            $model = self::orderBy($order, $dir);
        } else {
            $model = self::orderBy('id');
        }
        $model = $model->select('id','status','user_name','user_email'
                    ,'user_phone', 'grandtotal', 'created_at');
        $request = app('request')->input('filter');
        if ($request && count($request)) {
            foreach ($request as $key => $value) {
                if (is_array($value)) {
                    if (isset($value['from']) && $value['from']) {
                        $model = $model->where($key,'>=',$value['from']);
                    }
                    if (isset($value['to']) && $value['to']) {
                        $model = $model->where($key,'<=',$value['to']);
                    }
                    continue;
                }
                $model = $model->where($key,'like',"%$value%");
            }
        }
        $model = $model->paginate($pager['limit']);
        return $model;
    }
}
