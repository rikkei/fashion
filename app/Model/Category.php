<?php
namespace App\Model;

use DB;

class Category extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'category';
    protected $fillable = array('name', 'url_key', 'image','description','active','parent_id','position');
    
    /**
     * call back before save
     */
    protected static function boot()
    {
        static::creating(function ($model) {
            if(!$model->url_key) {
                $model->url_key = \App\Helpers\Config::urlSlug($model->name);
            }
            return true;
        });
        
    }
    
    /**
     * get collection to show grid
     * 
     * @return type
     */
    public static function getAllGrid()
    {
        $pager = \App\Helpers\Config::getPagerOption();
        $model = self::select('id','name','active','parent_id','position')->orderBy('id');
        $request = app('request')->input('filter');
        if ($request && count($request)) {
            foreach ($request as $key => $value) {
                if (is_array($value)) {
                    if (isset($value['from']) && $value['from']) {
                        $model = $model->where($key,'>=',$value['from']);
                    }
                    if (isset($value['to']) && $value['to']) {
                        $model = $model->where($key,'<=',$value['to']);
                    }
                    continue;
                }
                $model = $model->where($key,'like',"%$value%");
            }
        }
        $model = $model->paginate($pager['limit']);
        return $model;
    }

    /**
     * Overide Update the model in the database.
     *
     * @param  array  $attributes
     * @param  array  $options
     * @return bool|int
     */
    public function update(array $attributes = [], array $options = [])
    {
        if(isset($attributes['url_key'])) {
            if (!$attributes['url_key']) {
                $attributes['url_key'] = $attributes['name'];
            }
            $attributes['url_key'] = \App\Helpers\Config::urlSlug($attributes['url_key']);
        }
        return parent::update($attributes, $options);
    }
    
    /**
     * override delete model - delete all child of category
     */
    public function delete() {
        $id = $this->id;
        parent::delete();
        $this->deleteRecursive($id);
    }
    
    /**
     * delete recursive all child
     * 
     * @param type $id
     * @return type
     */
    protected function deleteRecursive($id)
    {
        if (!$id) {
            return;
        }
        $categoryCollection = Category::where('parent_id','=',$id)->get();
        if($categoryCollection->count()) {
            foreach($categoryCollection as $categoryChild) {
                $idChild = $categoryChild->id;
                Category::find($idChild)->delete();
                $this->deleteRecursive($idChild);
            }
        }
    }

    /**
     * get url request of category
     *
     * @param $id
     * @return string
     */
    public static function getUrl($id)
    {
        $itemRewrite = DB::table('url_rewrite')
            ->where('type', '=', 'category')
            ->where('item_id', '=', $id)
            ->orderBy('id', 'desc')
            ->first();
        if (count($itemRewrite)) {
            return url($itemRewrite->request_path);
        }
        return url('category/'.$id);
    }

    /**
     * get path url key
     *
     * @param $id
     * @return string
     */
    public static function getPathUrlKey($category)
    {
        $url = '';
        if(!is_object($category)) {
            $category = DB::table('category')
                ->select('id', 'url_key', 'parent_id')
                ->where('id', '=', $category)
                ->first();
        }
        if (count($category)) {
            $url .= '/' . $category->url_key;
            if($category->parent_id == 0) {
                return $url;
            }
            $url .= self::getPathUrlKey($category->id);
        }
        return $url;
    }
}
