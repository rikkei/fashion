<?php
namespace App\Model;

use DB;

class CategoryProduct extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'category_product';
    protected $fillable = array('category_id', 'product_id');
    
    /**
     * delete all product
     * 
     * @param type $productId
     */
    public static function deleteProduct($productId)
    {
        self::where('product_id', '=', $productId)->delete();
    }
    
    /**
     * delete all category
     * 
     * @param type $categoryId
     */
    public static function deleteCategory($categoryId)
    {
        self::where('category_id', '=', $categoryId)->delete();
    }
    
    /**
     * add item product to categories
     * 
     * @param type $productId
     * @param type $categoryIds
     */
    public static function addItemsCategory($productId, $categoryIds = null)
    {
        self::deleteProduct($productId);
        if($categoryIds && count($categoryIds)) {
            $dataInsert = array();
            foreach ($categoryIds as $categoryId) {
                $dataInsert[] = ['category_id' => $categoryId, 'product_id' => $productId];
            }
            DB::table('category_product')->insert($dataInsert);
        }
    }
    
    /**
     * get all category of product
     * 
     * @param type $productId
     * @return type
     */
    public static function getCategory($productId)
    {
        $category = self::select('category_id')->where('product_id', '=', $productId)->get();
        $result = array();
        if ($category && count($category)) {
            foreach ($category as $item) {
                $result[] = $item->category_id;
            }
        }
        return $result;
    }
    
    /**
     * get all product of category
     * 
     * @param type $categoryId
     * @return type
     */
    public static function getProduct($categoryId)
    {
        $collection = self::select('product_id')
                ->where('product_id', '=', $categoryId)
                ->get();
        $result = array();
        if ($collection && count($collection)) {
            foreach ($collection as $item) {
                $result[] = $item->product_id;
            }
        }
        return $result;
    }
}
