<?php
namespace App\Helpers;

use App\Model\Category;

class Menu extends BaseHelper
{
    /**
     * get Html Menu
     * 
     * @return html code
     */
    public static function getHtml()
    {
        $html = '';
        $html .= static::getMenuRecursive();
        return $html;
    }
    
    /**
     * get html menu recursice - call all sub category
     * 
     * @param type $id
     * @param type $level
     * @return string
     */
    protected static function getMenuRecursive($id = 0, $level = 0)
    {
        $html = '';
        $categoryCollection = Category::select('category.id as id','name','request_path')
            ->leftJoin('url_rewrite', function($join) {
                $join->on('url_rewrite.item_id', '=', 'category.id')
                    ->where('url_rewrite.type', '=', 'category');
            })
            ->where('parent_id','=',$id)
            ->where('active', '=', 1)
            ->groupBy('category.id')
            ->orderBy('position')
            ->get();
        if($categoryCollection->count()) {
            foreach($categoryCollection as $categoryChild) {
                $id = $categoryChild->id;
                $requestPath = $categoryChild->request_path;
                if (!$requestPath) {
                    $requestPath = 'category/'.$id;
                }
                $requestPath = url($requestPath);
                $htmlDropdown = self::getMenuRecursive($id, $level+1);
                $htmlClassLi = "li-level-{$level}";
                $htmlClassA = '';
                $htmlAttributeA = '';
                $appendTextA = '';

                if($htmlDropdown) {
                    $htmlClassA .= 'dropdown-toggle';
                    $htmlClassUl = "ul-level-{$level} dropdown-menu";
                    $htmlDropdownPrepend = "<ul class=\"{$htmlClassUl}\">";
                    $htmlDropdownPrepend .= '<li><a href="'.$requestPath.'">View all</a></li>';
                    $htmlDropdownPrepend .= $htmlDropdown;
                    $htmlDropdownPrepend .= '</ul>';
                    $htmlDropdown = $htmlDropdownPrepend;
                    $htmlAttributeA .= ' data-toggle="dropdown"';
                    if ($level == 0) {
                        $appendTextA = '<b class="caret"></b>';
                    }
                    if ($level > 0) {
                        $htmlClassLi .= ' dropdown-submenu';
                    }
                }
                $urlA = $requestPath;
                $html .= "<li class=\"{$htmlClassLi}\">";
                $html .= "<a class=\"{$htmlClassA}\"{$htmlAttributeA} href=\"{$urlA}\">";
                $html .= $categoryChild->name.$appendTextA;
                $html .= "</a>";
                $html .= $htmlDropdown;
                $html .= "</li>";
            }
        }
        return $html;
    }
}
