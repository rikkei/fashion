<?php
namespace App\Helpers;

class Output extends BaseHelper
{
    /**
     * get output price format
     * 
     * @param type $price
     * @return string
     */
    public static function priceProduct($item)
    {
        $price = (float)$item->price;
        $price = number_format($price, 2, '.', ',');
        $symbolCurrency = '$';
        if (!isset($item->special_price) || !$item->special_price) {
            $specialPrice = 0;
        } else {
            $specialPrice = (float)($item->special_price);
            $specialPrice = number_format($specialPrice, 2, '.', ',');
        }   
        $html = "<div class=\"price-{$item->id} price-box\">";
        if(!$specialPrice) {
            $html .= '<span class="price">';
            $html .= $symbolCurrency . $price;
            $html .= '</span>';
        } else {
            $html .= '<span class="price-origin">';
            $html .= $symbolCurrency . $price;
            $html .= '</span>';
            $html .= '<span class="price price-final">';
            $html .= $symbolCurrency . $specialPrice;
            $html .= '</span>';
        }
        $html .= '</div>';
        return $html;   
    }
    
    /**
     * price format output
     * 
     * @param type $price
     * @return price
     */
    public static function price($price)
    {
        $symbolCurrency = '$';
        $price = (float)$price;
        $price = number_format($price, 2, '.', ',');
        $price = $symbolCurrency.$price;
        $html = "<span class=\"price\">{$price}</span>";
        return $html;
    }
    
    /**
     * date format
     * 
     * @param type $date: format full: Y-m-d H:i:s
     * @param type $format
     * @return string
     */
    public static function date($date, $format = 'Y-m-d')
    {
        $date = date_create($date);
        return date_format($date, $format);
    }
}