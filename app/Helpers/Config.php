<?php
namespace App\Helpers;

class Config extends BaseHelper {

    public static function urlSlug($text) 
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }
    
    /**
     * Get value of input after submit or edit Model
     * 
     * @param type $model - model
     * @param type $key - key of value
     * @param type $keyArray - if key is array, prefix this
     * @param type $key2 - if key1 return null data, use this
     * @return type 
     */
    public static function getValueForm($model, $key ,$keyArray = null, $key2 = null)
    {
        if(isset($model) && isset($model->id) && $model) {
            if (!$model->{$key} && $key2) {
                return $model->{$key2};
            }
            return $model->{$key};
        } else {
            if($keyArray) {
                $key = $keyArray.'.'.$key;
            }
            if (old($key)) {
                return old($key);
            }
            return app('request')->input($key);
        }
    }
    
    /**
     * support upload file
     * 
     * @param type $file
     * @param type $path
     * @param type $prefix
     * @return type
     */
    public static function uploadFile($file, $path, $prefix = null)
    {
        $filename = $file->getClientOriginalName();
        if ($prefix) {
            $filename = $prefix.$filename;
        }
        $result = $file->move($path, $filename);
        return $result->getPathName();
    }
    
    /**
     * rebuild url with new params
     * 
     * @param type $paramsNew
     * @return type
     */
    public static function urlReplaceParams($paramsNew = array())
    {
        $request = app('request');
        if (!$paramsNew) {
            return $request->fullUrl();
        }
        $paramUrl = array_merge($_GET,$paramsNew);
        return $request->fullUrlWithQuery($paramUrl);
    }
    
    /**
     * get pager option 
     * 
     * @return type
     */
    public static function getPagerOption()
    {
        //$request = app('request');
        $pager = [
            'limit' => 10,
            'order' => 'id',
            'dir' => 'asc',
        ];
        $pager = array_merge($pager,$_GET);
        return $pager;
    }
    
    /**
     * get url order grid
     * 
     * @param type $orderKey
     * @return type
     */
    public static function getUrlOrder($orderKey)
    {
        $request = app('request');
        if (!$orderKey) {
            return $request->fullUrl();
        }
        $dir = null;
        if (!isset($_GET['order']) || $_GET['order'] != $orderKey) {
            $dir = 'desc';
        }
        elseif (isset($_GET['dir'])) {
            if ($_GET['dir'] == 'asc') {
                $dir = 'desc';
            } else {
                $dir = 'asc';
            }
        } else {
            $dir = 'desc';
        }
        $orderNew = [
            'order' => $orderKey,
            'dir' => $dir,
        ];
        $paramUrl = array_merge($_GET,$orderNew);
        return $request->fullUrlWithQuery($paramUrl);
    }
    
    /**
     * get direction class for current order
     * 
     * @param type $orderKey
     * @return type
     */
    public static function getDirClass($orderKey)
    {
        if (!isset($_GET['order']) || $_GET['order'] != $orderKey) {
            return '';
        }
        if(isset($_GET['dir']) && $_GET['dir'] == 'asc') {
            return 'sorting_asc';
        } else {
            return 'sorting_desc';
        }
    }
}
