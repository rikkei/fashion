<?php
namespace App\Helpers;

use Illuminate\Contracts\Support\Arrayable;

class Model extends BaseHelper 
{
    /**
     * convert model collection to array with key is code value
     * 
     * @param type $collection
     * @return Arrayable
     */
    public static function toArrayCodeKey($collection) {
        $result = array();
        foreach ($collection as $item) {
            if ($item instanceof Arrayable) {
                $result[$item->code] = $item->toArray();
            }
            else {
                $result[] = $item;
            }
        }
        return $result;
    }
}
