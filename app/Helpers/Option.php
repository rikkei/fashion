<?php
namespace App\Helpers;

use DB;

class Option extends BaseHelper
{
    protected static $storeOption = array();
    
    /**
     * Store option
     * 
     * @param type $key
     * @return boolean
     */
    protected static function storeOptionData($key)
    {
        if(isset(self::$storeOption[$key]) && self::$storeOption[$key]) {
            return self::$storeOption[$key];
        }
        return false;
    }

    /**
     * Yes - no Option
     * 
     * @return type
     */
    public static function yesNo()
    {
        if(self::storeOptionData(__FUNCTION__)) {
            return self::storeOptionData(__FUNCTION__);
        }
        self::$storeOption[__FUNCTION__] = [
            ['value'=>'1','label'=>'Yes'],
            ['value'=>'0','label'=>'No'],
        ];
        return self::$storeOption[__FUNCTION__];
    }
    
    /**
     * Category tree
     * 
     * @return type
     */
    public static function categoryTree($valueNull = true)
    {
        $key = __FUNCTION__.$valueNull;
        if(self::storeOptionData($key)) {
            return self::storeOptionData($key);
        }
        if ($valueNull) {
            self::$storeOption[$key][] = [
                'label' => ' ',
                'value' => '',
            ];
        }
        self::categoryTreeRecursive(0, 0, $key);
        return self::$storeOption[$key];
    }
    
    /**
     * get category tree option recursive
     * 
     * @param type $id
     * @param type $level
     * @param type $key
     */
    protected static function categoryTreeRecursive($id = null, $level = 0, $key = null) {
        $categoryCollection = \App\Model\Category::where('parent_id','=',$id)->orderBy('position')->get();
        $prefixLabel = '';
        for($i = 0 ; $i < $level ; $i++) {
            $prefixLabel .= '----&nbsp;&nbsp;';
        }
        if($categoryCollection->count()) {
            foreach($categoryCollection as $categoryChild) {
                if(Registry::hasLogger('current_category')) {
                    if(Registry::getInstance('current_category')->id == $categoryChild->id) {
                        continue;
                    }
                }

                self::$storeOption[$key][] = [
                    'label' => $prefixLabel.$categoryChild->name . ' (id:'.$categoryChild->id.')',
                    'value' => $categoryChild->id,
                ];
                self::categoryTreeRecursive($categoryChild->id,$level+1, $key);
            }
        }
    }
    
    /**
     * Category tree
     * 
     * @return type
     */
    public static function getTreeFull()
    {
        $key = __FUNCTION__;
        if(self::storeOptionData($key)) {
            return self::storeOptionData($key);
        }
        self::getTreeFullRecursive(0, 0, $key);
        return self::$storeOption[$key];
    }
    
    /**
     * get category tree option recursive
     * 
     * @param type $id
     * @param type $level
     * @param type $key
     */
    protected static function getTreeFullRecursive($id = null, $level = 0, $key = null) {
        $categoryCollection = \App\Model\Category::where('parent_id','=',$id)
                ->orderBy('position')->get();
        $prefixLabel = '';
        for($i = 0 ; $i < $level ; $i++) {
            $prefixLabel .= '----&nbsp;&nbsp;';
        }
        if (!$categoryCollection->count()) {
            return;
        }
        foreach($categoryCollection as $categoryChild) {
            self::$storeOption[$key][] = [
                'label' => $prefixLabel.$categoryChild->name . ' (id:'.$categoryChild->id.')',
                'value' => $categoryChild->id,
            ];
            self::getTreeFullRecursive($categoryChild->id, $level+1, $key);
        }
    }
    
    /**
     * limt Option
     * 
     * @return type
     */
    public static function limit()
    {
        if(self::storeOptionData(__FUNCTION__)) {
            return self::storeOptionData(__FUNCTION__);
        }
        self::$storeOption[__FUNCTION__] = [
            ['value'=>'10','label'=>'10'],
            ['value'=>'20','label'=>'20'],
            ['value'=>'50','label'=>'50'],
            ['value'=>'2','label'=>'2'],
            ['value'=>'1','label'=>'1'],
        ];
        return self::$storeOption[__FUNCTION__];
    }
    
    /**
     * Category tree checkbox
     * 
     * @return type
     */
    public static function categoryTreeCheckbox($category = array())
    {
        if(self::storeOptionData(__FUNCTION__)) {
            return self::storeOptionData(__FUNCTION__);
        }
        self::categoryTreeRecursiveCheckbox(0, 0,$category);
        return self::$storeOption[__FUNCTION__];
    }
    
    protected static function categoryTreeRecursiveCheckbox($id = null, $level = 0, $category = array()) {
        $categoryCollection = \App\Model\Category::where('parent_id','=',$id)->orderBy('position')->get();
        $prefixLabel = '';
        for($i = 0 ; $i < $level ; $i++) {
            $prefixLabel .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
        }
        if($categoryCollection->count()) {
            foreach($categoryCollection as $categoryChild) {
                if ($category && in_array($categoryChild->id, $category)) {
                    $checked = ' checked';
                } else {
                    $checked = '';
                }
                $label = $prefixLabel . "<input type=\"checkbox\" name=\"category[]\" value=\"{$categoryChild->id}\" id=\"category-{$categoryChild->id}\"{$checked} />";
                $label .= "&nbsp;<label for=\"category-{$categoryChild->id}\">{$categoryChild->name} (id: $categoryChild->id)</label>";
                self::$storeOption['categoryTreeCheckbox'][] = [
                    'label' => $label,
                    'value' => $categoryChild->id,
                ];
                self::categoryTreeRecursiveCheckbox($categoryChild->id,$level+1,$category);
            }
        }
    }
    
    /**
     * blog category Option
     * 
     * @return type
     */
    public static function blogCategory()
    {
        if(self::storeOptionData(__FUNCTION__)) {
            return self::storeOptionData(__FUNCTION__);
        }
        self::$storeOption[__FUNCTION__][] = [
            'label' => ' ',
            'value' => '',
        ];
        $collection = \App\Model\BlogCategory::select('id', 'name')
                ->orderBy('id')
                ->get();
        if (count($collection)) {
            foreach ($collection as $item) {
                self::$storeOption[__FUNCTION__][] = [
                    'label' => $item->name,
                    'value' => $item->id,
                ];
            }
        }
        return self::$storeOption[__FUNCTION__];
    }
}
