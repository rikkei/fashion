<?php

namespace App\Exceptions;

use App\Model\UrlRewrite;
use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Route;
use Request;
use DB;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if ($this->isHttpException($e)) {
           $status = $e->getStatusCode();
            switch ($status) {
                case '404':
                    return $this->process404($request ,$e);
            }
        }
        return parent::render($request, $e);
    }

    protected function process404($request, $e)
    {
        $routeCollection = Route::getRoutes();
        $path = trim($request->path(),'/');
        $item = UrlRewrite::loadByRequestPath($path);
        if (!count($item)) {
            $route = $routeCollection->getByName('404');
            $route->bindParameters($request);
        } elseif (!$routeCollection->getByName($item->type)) {
            $route = $routeCollection->getByName('404');
            $route->bindParameters($request);
        } else {
            $route = $routeCollection->getByName($item->type);
            $route->bindParameters($request);
            $params = $item->params;
            $itemId = $item->item_id;
            $params = explode('/', $params);
            if ($itemId) {
                $route->setParameter('id', $itemId);
            }
            if (count($params)) {
                for ($i = 0 ; $i < count($params) ; $i += 2) {
                    if (isset($params[$i]) && isset($params[$i+1])) {
                        $route->setParameter($params[$i],$params[$i+1]);
                    }
                }
            }
        }
        return $route->run($request);
    }
}
