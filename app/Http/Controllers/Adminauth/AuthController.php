<?php

namespace App\Http\Controllers\Adminauth;

use App\Model\Admin;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Auth;
use Redirect;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo;
	protected $guard = 'admin';

    public function __construct() 
    {
        $this->redirectTo = config('base.admin_uri');
        return $this;
    }
    
    /**
     * Redirect if admin logged in
     * 
     * @return type
     */
    protected function redirectIfLogged()
    {
        if(Auth::guard($this->guard)->check()) {
            return Redirect::to($this->redirectTo)->send(); 
        }
    }

    /**
     * admin login form
     * 
     * @return type
     */
    public function loginView()
    {
        $this->redirectIfLogged();
        return view('admin.auth.login');
    }
    
    /*public function registerView()
    {
        return view('admin.auth.register');
    }*/
    
    public function resetPassword()
	{
		return view('admin.auth.passwords.email');
	}
	
	public function logout(){
		Auth::guard('admin')->logout();
		return redirect('/'.$this->redirectTo.'/login');
	}

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return Admin::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
}
