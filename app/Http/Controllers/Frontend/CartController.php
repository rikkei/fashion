<?php
namespace App\Http\Controllers\Frontend;

use App\Http\Requests;
use App\Model\Category;
use App\Model\Product;
use App\Helpers\Registry;
use Request;
use Validator;

class CartController extends FrontendController
{
    /**
     * Show Cart page
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->breadcrumb['cart'] = [
            'text' => 'Cart'
        ];
        return view('cart.index',[
            'breadcrumb' => $this->breadcrumb,
            'title' => 'Cart',
            'pageTitle' => 'Cart',
            'items' => $this->getCartItems(),
            'totalPrice' => $this->getTotalPrice(),
        ]);
    }
    
    /**
     * Cart add action
     * 
     */
    public function add()
    {
        $request = app('request');
        $qty = $request->input('qty');
        if (!is_numeric($qty) || $qty < 1) {
            $qty = 1;
        }
        $id = $request->input('id');
        $product = Product::loadItem($id, ['name', 'price', 'special_price']);        
        
        if(!$product || !count($product)) {
            return redirect()->back();
        }
        $cartItems = $this->getCartItems();
        if (!isset($cartItems[$id])) {
            $cartItems[$id] = array();
        }
        $cartItemOldData = $this->getCartItem($id);
        $qty += (isset($cartItemOldData['qty']) && $cartItemOldData['qty']) ? $cartItemOldData['qty'] : 0;
        $cartItemNewData = [
            'name' => $product->name,
            'price' => $product->final_price,
            'qty' => $qty,
            'sku' => $product->sku,
        ];
        $this->setCartItem($id, $cartItemNewData);
        $messages = array('success'=> [
                            'Add to cart success!',
                        ],);
        session(['messages' => $messages]);
        return redirect()->back();
    }
    
    /**
     * route delete item cart
     * 
     * @param type $id
     * @param type $token
     * @return type
     */
    public function delete($id, $token)
    {
        if (csrf_token() != $token) {
            return redirect('cart')->withErrors('Error token key!');
        }
        $this->removeCartItem($id);
        $messages = array('success'=> [
                        'Remove item success!',
                    ],);
        return redirect('cart')->with('messages',$messages);
    }
    
    /**
     * route checkout submit
     * 
     * @return type
     */
    public function checkout()
    {
        $request = app('request');
        $orderUser = $request->input('order');
        $validator = Validator::make($orderUser, [
            'user_name' => 'required|max:255',
            'user_email' => 'required|email|max:255',
            'user_phone' => 'required|max:15',
            'user_address' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect('cart')
                ->withInput()
                ->withErrors($validator);
        }
        if (!$this->getCartItems()) {
            redirect('cart')->withErrors('Cart is empty');
        }
        $orderUser['subtotal'] = $this->getTotalPrice();
        $orderUser['grandtotal'] = $this->getTotalPrice();
        $orderUser['status'] = 'new';
        try{
            $order = \App\Model\Order::create($orderUser);
            $orderId = $order->id;
            foreach ($this->getCartItems() as $id => $item) {
                \App\Model\OrderItem::create([
                    'order_id' => $orderId,
                    'product_id' => $id,
                    'name' => $item['name'],
                    'sku' => $item['sku'],
                    'qty' => $item['qty'],
                    'price' => $item['price'],
                ]);
            }
            app('session')->push('order',$order);
            $this->removeCart();
            return redirect('cart/success');
        } catch (Exception $ex) {
            redirect('cart')->withErrors($ex);
        }
    }
    
    /**
     * route success page
     * 
     * @return type
     */
    public function success()
    {
        $this->breadcrumb['success'] = [
            'text' => 'Success'
        ];
        
        $order = app('session')->pull('order');
        if(!$order) {
            return redirect('/');
        }
        return view('cart.success',[
            'breadcrumb' => $this->breadcrumb,
            'title' => 'Success',
            'pageTitle' => 'Success',
            'order' => $order[0],
        ]);
    }
    /**
     * get info cart
     * 
     * @return cart
     */
    protected function getCart()
    {
        return session('cart');
    }
    
    /**
     * get all item in cart
     * 
     * @return array
     */
    protected function getCartItems()
    {
        $cartItems = session('cart.items');
        if ($cartItems === null) {
            session(['cart.items' => array()]);
            $cartItems = array();
        }
        return $cartItems;
    }
    
    /**
     * get item in cart
     * 
     * @param type $id
     * @return array
     */
    protected function getCartItem($id)
    {
        $cartItem = session('cart.items.'.$id);
        if ($cartItem === null) {
            $cartItem = array();
            session(['cart.items.'.$id => array('qty' => 0)]);
        }
        return $cartItem;
    }
    
    /**
     * set information of item in cart
     * 
     * @param type $id
     * @param type $data
     * @return array
     */
    protected function setCartItem($id, $data)
    {
        $cartItem = session(['cart.items.'.$id => $data]);
        $this->calculatorCart();
        return $cartItem;
    }
    
    protected function removeCartItem($id)
    {
        app('session')->forget('cart.items.'.$id);
        $this->calculatorCart();
    }

    protected function removeCart()
    {
        app('session')->forget('cart.items');
        $this->calculatorCart();
    }

    /**
     * calculator total price
     * 
     * @return float
     */
    protected function calculatorCart()
    {
        $items = $this->getCartItems();
        $total = 0;
        if (count($items)) {
            foreach ($items as $item) {
                if(isset($item['price']) && isset($item['qty'])) {
                    $total += $item['price'] * $item['qty'];
                }
            }
        }
        session(['cart.price'=>$total]);
        return $total;
    }
    
    /**
     * get total price in cart
     * 
     * @return float
     */
    protected function getTotalPrice()
    {
        return session('cart.price');
    }
}


