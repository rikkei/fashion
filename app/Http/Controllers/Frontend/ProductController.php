<?php
namespace App\Http\Controllers\Frontend;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Model\Product;
use App\Helpers\Registry;

class ProductController extends FrontendController
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $product = Product::find($id);
        if(!count($product)) {
            return $this->page404();
        }
        $product = Product::loadItem($product);
        Registry::addLogger($product,'current_product');
        $this->breadcrumb['product'] = [
            'text' => $product->name,
        ];
        $gallery = \App\Model\Gallery::getGalleryProduct($id);
        return view('product.index',[
            'breadcrumb' => $this->breadcrumb,
            'title' => $product->name,
            'pageTitle' => $product->name,
            'product' => $product,
            'gallery' => $gallery,
        ]);
    }
}


