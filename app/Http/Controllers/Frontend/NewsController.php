<?php
namespace App\Http\Controllers\Frontend;

use App\Model\News;

class Newscontroller extends FrontendController
{
    /**
     * Show the list view
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $collection = News::getList();
        $this->breadcrumb['news'] = [
            'text' => 'News',
        ];
        return view('news.list',[
            'breadcrumb' => $this->breadcrumb,
            'title' => 'News',
            'pageTitle' => 'News',
            'model' => $collection,
        ]);
    }
    
    public function view($id)
    {
        $model = News::find($id);
        if (!count($model)) {
            return $this->page404();
        }
        $this->breadcrumb['news'] = [
            'text' => 'News',
            'url' => url('/news'),
        ];
        $this->breadcrumb['view'] = [
            'text' => $model->name,
        ];
        return view('news.view',[
            'breadcrumb' => $this->breadcrumb,
            'title' => $model->name,
            'pageTitle' => $model->name,
            'model' => $model,
        ]);
    }
}

