<?php
namespace App\Http\Controllers\Frontend;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Model\Category;
use App\Model\Product;
use App\Helpers\Registry;

class CategoryController extends FrontendController
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $category = Category::find($id);
        if(!count($category)) {
            return $this->page404();
        }
        Registry::addLogger($category,'current_category');
        $this->breadcrumb['category'] = [
            'text' => $category->name,
        ];
        $product = Product::getProductCategory($id, true);
        return view('category.index',[
            'breadcrumb' => $this->breadcrumb,
            'title' => $category->name,
            'pageTitle' => $category->name,
            'category' => $category,
            'model' => $product,
        ]);
    }
}

