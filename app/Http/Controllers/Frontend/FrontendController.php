<?php
namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Model\Category;

class FrontendController extends Controller
{
    protected $guard = 'web';
    
    /**
     * construct
     */
    public function __construct()
    {
        $this->breadcrumb = [
            'home' => [
                'url' => url('/'),
                'text' => 'Home',
            ],
        ];
    }
}

