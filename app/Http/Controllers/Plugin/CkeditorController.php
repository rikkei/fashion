<?php
namespace App\Http\Controllers\Plugin;

use App\Http\Controllers\Controller;

class CkeditorController extends Controller
{
    public function browse()
    {
        return view('admin.ckeditor.cfinder',[
            'title' => 'File Manager',
        ]);
    }
    
    public function upload()
    {
        echo 'upload';
    }
}

