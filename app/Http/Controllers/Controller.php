<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;
    
    public function page404()
    {
        $this->breadcrumb['404'] = [
            'text' => '404 page',
        ];
        return view('404', [
            'breadcrumb' => $this->breadcrumb,
            'title' => '404 Page',
        ]);
    }
}
