<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Model\Order;
use Validator;
use App\Helpers\Config;
use App\Helpers\Registry;

class OrderController extends AdminController
{
    protected $breadcrumb;
    
    /**
     * List order
     *
     * @return void
     */
    public function index(Request $request)
    {
        $this->breadcrumb['order'] = [
            'text'=> 'Order',
        ];
        $order = Order::getAllGrid();
        return view('admin.order.list',[
            'breadcrumb' => $this->breadcrumb,
            'title' => 'Order',
            'pageTitle' => 'Order',
            'menuActive' => 'order',
            'model' => $order,
        ]);
    }
    
    
    
    /**
     * Edit order 
     *
     * @return void
     */
    public function edit($id)
    {
        $order = Order::find($id);
        if(!count($order)) {
            return redirect(config('base.admin_uri').'/order')->withErrors('Not found Order');
        }
        $items = \App\Model\OrderItem::where('order_id', '=', $id)->get();
        $this->breadcrumb['order'] = [
            'text' => 'Order',
            'url' => url(config('base.admin_uri').'/order'),
        ];
        $this->breadcrumb['edit'] = [
            'text' => $id,
        ];
        return view('admin.order.edit',[
            'breadcrumb' => $this->breadcrumb,
            'title' => 'Edit Order: '. $id,
            'pageTitle' => 'Edit Order: '. $id,
            'menuActive' => 'order',
            'model' => $order,
            'items' => $items,
        ]);
    }

}
