<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Model\News;
use Validator;

class NewsController extends AdminController
{
    protected $breadcrumb;
    
    /**
     * List news
     *
     * @return void
     */
    public function index(Request $request)
    {
        $this->breadcrumb['news'] = [
            'text'=> 'News',
        ];
        $news = News::getAllGrid();
        return view('admin.news.list',[
            'breadcrumb' => $this->breadcrumb,
            'title' => 'News',
            'pageTitle' => 'News',
            'menuActive' => 'News',
            'model' => $news,
        ]);
    }
    
    /**
     * Create new news
     *
     * @return void
     */
    public function create()
    {
        $this->breadcrumb['news'] = [
            'text' => 'News',
            'url' => url(config('base.admin_uri').'/news'),
        ];
        $this->breadcrumb['new'] = [
            'text' => 'Add new',
        ];
        return view('admin.news.edit',[
            'breadcrumb' => $this->breadcrumb,
            'title' => 'Add new News',
            'pageTitle' => 'Add new News',
            'menuActive' => 'news',
            'model' => null,
        ]);
    }
    
    /**
     * Save new news
     *
     * @return void
     */
    public function createPost(Request $request)
    {
        try{
            $input = $request->input('item');
            if(!$input['url_key']) {
                $input['url_key'] = \App\Helpers\Config::urlSlug($input['name']);
            }
            $validator = Validator::make($input, [
                'name' => 'required|max:255',
                'url_key' => 'required|max:255|unique:news',
                'created_at' => 'date_format:Y-m-d H:i:s',
                
            ]);
            if ($validator->fails()) {
                return redirect(config('base.admin_uri').'/news/create')
                    ->withInput()
                    ->withErrors($validator);
            }
            if (isset($input['created_at']) && !$input['created_at']) {
                unset($input['created_at']);
            }
            $model = News::create($input);
            $id = $model->id;
            $messages = array('success'=> [
                            'Create News success!',
                        ],);
            if($request->input('submit_continue')) {
                return redirect(config('base.admin_uri').'/news/edit/'.$id)
                    ->with('messages',$messages);
            }
            return redirect(config('base.admin_uri').'/news/')
                    ->with('messages',$messages);        
        } catch (Exception $ex) {
            return redirect(config('base.admin_uri').'/news/create')->withErrors($ex)->withInput();
        }
    }
    
    /**
     * Edit category
     *
     * @return void
     */
    public function edit($id)
    {
        $news = News::find($id);
        if(!count($news)) {
            return redirect(config('base.admin_uri').'/news')->withErrors('Not found News');
        }
        $this->breadcrumb['news'] = [
            'text' => 'News',
            'url' => url(config('base.admin_uri').'/news'),
        ];
        $this->breadcrumb['edit'] = [
            'text' => $news->name,
        ];
        return view('admin.news.edit',[
            'breadcrumb' => $this->breadcrumb,
            'title' => 'Edit News: '.$news->name,
            'pageTitle' => 'Edit News: '.$news->name,
            'menuActive' => 'news',
            'model' => $news,
        ]);
    }
    
    /**
     * Save category
     *
     * @return void
     */
    public function editPost(Request $request,$id)
    {
        try{
            $model = News::find($id);
            if (!count($model)) {
                return redirect(config('base.admin_uri').'/news')->withErrors('Not found News');
            }
            $input = $request->input('item');
            if(!$input['url_key']) {
                $input['url_key'] = \App\Helpers\Config::urlSlug($input['name']);
            }
            $validator = Validator::make($input, [
                'name' => 'required|max:255',
                'url_key' => 'required|max:255|unique:news,url_key,'.$id,
                'created_at' => 'date_format:Y-m-d H:i:s',
                
            ]);
            if ($validator->fails()) {
                return redirect(config('base.admin_uri').'/news/edit/'.$id)
                    ->withInput()
                    ->withErrors($validator);
            }
            if (isset($input['created_at']) && !$input['created_at']) {
                unset($input['created_at']);
            }
            $model = $model->update($input);
            $messages = array('success'=> [
                            'Save News success!',
                        ],);
            if($request->input('submit_continue')) {
                return redirect(config('base.admin_uri').'/news/edit/'.$id)
                    ->with('messages',$messages);
            }
            return redirect(config('base.admin_uri').'/news/')
                    ->with('messages',$messages);        
        } catch (Exception $ex) {
            return redirect(config('base.admin_uri').'/news/edit/'.$id)->withErrors($ex)->withInput();
        }
    }
    
    public function delete(Request $request, $id, $token)
    {
        if (csrf_token() != $token) {
            return redirect(config('base.admin_uri').'/news/')->withErrors('Error token key!');
        }
        $model = News::find($id);
        if(!count($model)) {
            return redirect(config('base.admin_uri').'/news')->withErrors('Not found Item');
        }
        try {
            $model->delete();
            $messages = array('success'=> [
                            'Delete item success!',
                        ],);
            return redirect(config('base.admin_uri').'/news/')
                    ->with('messages',$messages);
        } catch (Exception $ex) {
            return redirect(config('base.admin_uri').'/blog/')->withErrors($ex);
        }
    }
    
    public function massAction(Request $request)
    {
        if (!count($request->input('item')) || !$request->input('item')) {
            return redirect(config('base.admin_uri').'/news/')->withErrors('Please choose item');
        }
        if ($request->input('action') == 'delete') {
            return $this->massDelete($request);
        }
        return redirect(config('base.admin_uri').'/news/');
    }
    
    protected function massDelete(Request $request)
    {
        try{
            foreach ($request->input('item') as $item) {
                $model = News::find($item);
                if(count($model)) {
                    $model->delete();
                }
            }
            $messages = array('success'=> [
                            'Mass delete success!',
                        ],);
            return redirect(config('base.admin_uri').'/news/')
                    ->with('messages',$messages);
        } catch (Exception $ex) {
            return redirect(config('base.admin_uri').'/news/')->withErrors($ex);
        }   
    }
}
