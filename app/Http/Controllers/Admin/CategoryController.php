<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Model\Category;
use Validator;
use App\Helpers\Config;
use App\Helpers\Registry;
use File;

class CategoryController extends AdminController
{
    protected $breadcrumb;
    
    /**
     * List category
     *
     * @return void
     */
    public function index(Request $request)
    {
        $this->breadcrumb['category'] = [
            'text'=> 'Category',
        ];
        return view('admin.category.view',[
            'breadcrumb' => $this->breadcrumb,
            'title' => 'Category',
            'pageTitle' => 'Category',
            'menuActive' => 'category',
            'model' => null,
        ]);
    }
    
    /**
     * Create new category
     *
     * @return void
     */
    public function create()
    {
        $this->breadcrumb['category'] = [
            'text' => 'Category',
            'url' => url(config('base.admin_uri').'/category'),
        ];
        $this->breadcrumb['new'] = [
            'text' => 'Add new',
        ];
        return view('admin.category.edit',[
            'breadcrumb' => $this->breadcrumb,
            'title' => 'Add new Category',
            'pageTitle' => 'Add new Category',
            'menuActive' => 'category',
            'model' => null,
        ]);
    }
    
    /**
     * Save new category
     *
     * @return void
     */
    public function createPost(Request $request)
    {
        try{
            $inputCategory = $request->input('category');
            $validator = Validator::make($inputCategory, [
                'name' => 'required|max:255',
            ]);
            if ($validator->fails()) {
                return redirect(config('base.admin_uri').'/category')
                    ->withInput()
                    ->withErrors($validator);
            }
            $category = Category::create($inputCategory);
            $id = $category->id;
            $messages = array('success'=> [
                            'Create Category success!',
                        ],);
            if($request->input('submit_continue')) {
                return redirect(config('base.admin_uri').'/category/edit/'.$id)
                    ->with('messages',$messages);
            }
            return redirect(config('base.admin_uri').'/category/')
                    ->with('messages',$messages);        
        } catch (Exception $ex) {
            return redirect(config('base.admin_uri').'/category')->withErrors($ex)->withInput();
        }
    }
    
    /**
     * Edit category
     *
     * @return void
     */
    public function edit($id)
    {
        $category = Category::find($id);
        if(!count($category)) {
            return redirect(config('base.admin_uri').'/category')->withErrors('Not found Category');
        }
        $products = \App\Model\CategoryProduct::getProduct($id);
        Registry::addLogger($category,'current_category',true);
        $this->breadcrumb['category'] = [
            'text' => 'Category',
            'url' => url(config('base.admin_uri').'/category'),
        ];
        $this->breadcrumb['edit'] = [
            'text' => $category->name,
        ];
        return view('admin.category.view',[
            'breadcrumb' => $this->breadcrumb,
            'title' => 'Edit Category: '.$category->name,
            'pageTitle' => 'Edit Category: '.$category->name,
            'menuActive' => 'category',
            'model' => $category,
            'products' => $products,
        ]);
    }
    
    /**
     * Save category
     *
     * @return void
     */
    public function editPost(Request $request,$id)
    {
        $category = Category::find($id);
        if(!count($category)) {
            return redirect(config('base.admin_uri').'/category')->withErrors('Not found Category');
        }
        try{
            $inputCategory = $request->input('category');
            $validator = Validator::make($inputCategory, [
                'name' => 'required|max:255',
            ]);
            if ($validator->fails()) {
                return redirect(config('base.admin_uri').'/category/edit/'.$id)
                    ->withInput()
                    ->withErrors($validator);
            }
            Category::find($id)->update($inputCategory);
            $messages = array('success'=> [
                            'Save Category success!',
                        ],);
            if($request->input('submit_continue')) {
                return redirect(config('base.admin_uri').'/category/edit/'.$id)
                    ->with('messages',$messages);
            }
            return redirect(config('base.admin_uri').'/category/')
                    ->with('messages',$messages);            
        } catch (Exception $ex) {
            return redirect(config('base.admin_uri').'/category/edit/'.$id)->withErrors($ex)->withInput();
        }
    }
    
    public function delete(Request $request, $id, $token)
    {
        if (csrf_token() != $token) {
            return redirect(config('base.admin_uri').'/category/')->withErrors('Error token key!');
        }
        $category = Category::find($id);
        if(!count($category)) {
            return redirect(config('base.admin_uri').'/category')->withErrors('Not found Item');
        }
        try {
            $category->delete();
            $messages = array('success'=> [
                            'Delete item success!',
                        ],);
            return redirect(config('base.admin_uri').'/category/')
                    ->with('messages',$messages);
        } catch (Exception $ex) {
            return redirect(config('base.admin_uri').'/category/')->withErrors($ex);
        }
    }
    
    public function massAction(Request $request)
    {
        if (!count($request->input('item')) || !$request->input('item')) {
            return redirect(config('base.admin_uri').'/category/')->withErrors('Please choose item');
        }
        if ($request->input('action') == 'delete') {
            $this->massDelete($request);
        }
        return redirect(config('base.admin_uri').'/category/');
    }
    
    protected function massDelete(Request $request)
    {
        try{
            foreach ($request->input('item') as $item) {
                $category = Category::find($item);
                if(count($category)) {
                    $category->delete();
                }
            }
            $messages = array('success'=> [
                            'Mass delete success!',
                        ],);
            return redirect(config('base.admin_uri').'/category/')
                    ->with('messages',$messages);
        } catch (Exception $ex) {
            return redirect(config('base.admin_uri').'/category/')->withErrors($ex);
        }   
    }
}
