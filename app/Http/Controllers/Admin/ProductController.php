<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Model\Product;
use App\Model\Attribute;
use Validator;
use App\Model\AttributeValue;
use App\Model\CategoryProduct;
use App\Model\Gallery;
use App\Helpers\Registry;

class ProductController extends AdminController
{
    protected $breadcrumb;
    
    /**
     * List category
     *
     * @return void
     */
    public function index(Request $request)
    {
        $this->breadcrumb['product'] = [
            'text'=> 'Product',
        ];
        $model = Product::getAllGrid();
        return view('admin.product.list',[
            'breadcrumb' => $this->breadcrumb,
            'title' => 'Product',
            'pageTitle' => 'Product',
            'menuActive' => 'product',
            'model' => $model,
        ]);
    }
    
    /**
     * Create new category
     *
     * @return void
     */
    public function create()
    {
        $this->breadcrumb['product'] = [
            'text' => 'Product',
            'url' => url(config('base.admin_uri').'/product'),
        ];
        $this->breadcrumb['new'] = [
            'text' => 'Add new',
        ];
        $attributes = Attribute::getAttributeEdit();
        return view('admin.product.edit',[
            'breadcrumb' => $this->breadcrumb,
            'title' => 'Add new Product',
            'pageTitle' => 'Add new Product',
            'menuActive' => 'product',
            'model' => null,
            'attributes' => $attributes,
            'categoryProduct' => null,
            'galleryProduct' => null,
        ]);
    }
    
    /**
     * Save new category
     *
     * @return void
     */
    public function createPost(Request $request)
    {
        try{
            $item = $request->input('item');
            $attribute = $request->input('attr');
            $category = $request->input('category');
            $validator = Validator::make($item, [
                'sku' => 'required|max:255|unique:product',
            ]);
            if ($validator->fails()) {
                return redirect(config('base.admin_uri').'/product/create')
                    ->withInput()
                    ->withErrors($validator);
            }
            
            $errors = Attribute::checkDataInput($attribute);
            if ($errors) {
                return redirect(config('base.admin_uri').'/product/create')
                    ->withInput()
                    ->withErrors($errors);
            }
            $model = Product::create($item);
            $id = $model->id;
            AttributeValue::createNew($attribute, $id);
            CategoryProduct::addItemsCategory($id, $category);
            $messages = array('success'=> [
                            'Create Product success!',
                        ],);
            if($request->input('submit_continue')) {
                return redirect(config('base.admin_uri').'/product/edit/'.$id)
                    ->with('messages',$messages);
            }
            return redirect(config('base.admin_uri').'/product/')
                    ->with('messages',$messages);        
        } catch (Exception $ex) {
            return redirect(config('base.admin_uri').'/product/create')->withErrors($ex)->withInput();
        }
    }
    
    /**
     * Edit category
     *
     * @return void
     */
    public function edit($id)
    {
        $model = Product::loadItem($id);
        if(!$model || !count($model)) {
            return redirect(config('base.admin_uri').'/product')->withErrors('Not found Product');
        }
        Registry::addLogger($model,'current_product',true);
        $this->breadcrumb['category'] = [
            'text' => 'Product',
            'url' => url(config('base.admin_uri').'/product'),
        ];
        $this->breadcrumb['edit'] = [
            'text' => $model->name,
        ];
        $attributes = Attribute::getAttributeEdit();
        $categoryProduct = CategoryProduct::getCategory($id);
        $galleryProduct = Gallery::getGalleryProduct($id,true);
        return view('admin.product.edit',[
            'breadcrumb' => $this->breadcrumb,
            'title' => 'Edit Product: '.$model->name,
            'pageTitle' => 'Edit Product: '.$model->name,
            'menuActive' => 'product',
            'model' => $model,
            'attributes' => $attributes,
            'categoryProduct' => $categoryProduct,
            'galleryProduct' => $galleryProduct,
        ]);
    }
    
    /**
     * Save category
     *
     * @return void
     */
    public function editPost(Request $request,$id)
    {
        $model = Product::find($id);
        if(!count($model)) {
            return redirect(config('base.admin_uri').'/product')->withErrors('Not found Product');
        }
        try{
            $item = $request->input('item');
            $attribute = $request->input('attr');
            $category = $request->input('category');
            $ngallery = $request->input('ngallery');
            $gallery = $request->input('gallery');
            $validator = Validator::make($item, [
                'sku' => 'required|max:255|unique:product,sku,'.$id,
            ]);
            if ($validator->fails()) {
                return redirect(config('base.admin_uri').'/product/edit/'.$id)
                    ->withInput()
                    ->withErrors($validator);
            }
            
            $errors = Attribute::checkDataInput($attribute,$id);
            if ($errors) {
                return redirect(config('base.admin_uri').'/product/edit/'.$id)
                    ->withInput()
                    ->withErrors($errors);
            }
            $model = Product::where('id', '=', $id)->update($item);
            AttributeValue::saveItem($attribute, $id);
            CategoryProduct::addItemsCategory($id, $category);
            Gallery::createNewGallery($id, $ngallery);
            Gallery::saveGallery($gallery);
            $messages = array('success'=> [
                            'Save Product success!',
                        ],);
            if($request->input('submit_continue')) {
                return redirect(config('base.admin_uri').'/product/edit/'.$id)
                    ->with('messages',$messages);
            }
            return redirect(config('base.admin_uri').'/product/')
                    ->with('messages',$messages);        
        } catch (Exception $ex) {
            return redirect(config('base.admin_uri').'/product/edit/'.$id)->withErrors($ex)->withInput();
        }
    }
    
    public function delete(Request $request, $id, $token)
    {
        if (csrf_token() != $token) {
            return redirect(config('base.admin_uri').'/product/')->withErrors('Error token key!');
        }
        $model = Product::find($id);
        if(!count($model)) {
            return redirect(config('base.admin_uri').'/product')->withErrors('Not found Item');
        }
        try {
            $model->delete();
            $messages = array('success'=> [
                            'Delete item success!',
                        ],);
            return redirect(config('base.admin_uri').'/product/')
                    ->with('messages',$messages);
        } catch (Exception $ex) {
            return redirect(config('base.admin_uri').'/product/')->withErrors($ex);
        }
    }
    
    public function massAction(Request $request)
    {
        if (!count($request->input('item')) || !$request->input('item')) {
            return redirect(config('base.admin_uri').'/category/')->withErrors('Please choose item');
        }
        if ($request->input('action') == 'delete') {
            $this->massDelete($request);
        }
        return redirect(config('base.admin_uri').'/category/');
    }
    
    protected function massDelete(Request $request)
    {
        try{
            foreach ($request->input('item') as $item) {
                $category = Category::find($item);
                if(count($category)) {
                    $category->delete();
                }
            }
            $messages = array('success'=> [
                            'Mass delete success!',
                        ],);
            return redirect(config('base.admin_uri').'/category/')
                    ->with('messages',$messages);
        } catch (Exception $ex) {
            return redirect(config('base.admin_uri').'/category/')->withErrors($ex);
        }   
    }
}
