<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Model\BlogComment;
use Validator;

class BlogCommentController extends AdminController
{
    protected $breadcrumb;
    
    /**
     * List news
     *
     * @return void
     */
    public function index(Request $request)
    {
        $this->breadcrumb['blog'] = [
            'text'=> 'Blog',
        ];
        $blog = BlogComment::getAllGrid();
        return view('admin.blog.comment.list',[
            'breadcrumb' => $this->breadcrumb,
            'title' => 'Blog Comment',
            'pageTitle' => 'Blog Comment',
            'menuActive' => 'Blog Comment',
            'model' => $blog,
        ]);
    }
    
    /**
     * Edit comment
     *
     * @return void
     */
    public function edit($id)
    {
        $model = BlogComment::find($id);
        if(!count($model)) {
            return redirect(config('base.admin_uri').'/blog/comment')
                    ->withErrors('Not found blog comment');
        }
        $this->breadcrumb['blog'] = [
            'text' => 'Blog Comment',
            'url' => url(config('base.admin_uri').'/blog/comment'),
        ];
        $this->breadcrumb['edit'] = [
            'text' => $model->id,
        ];
        return view('admin.blog.comment.edit',[
            'breadcrumb' => $this->breadcrumb,
            'title' => 'Edit comment: '.$model->id,
            'pageTitle' => 'Edit comment: '.$model->id,
            'menuActive' => 'blog',
            'model' => $model,
        ]);
    }
    
    /**
     * Save comment
     *
     * @return void
     */
    public function editPost(Request $request,$id)
    {
        try{
            $model = BlogComment::find($id);
            if (!count($model)) {
                return redirect(config('base.admin_uri').'/blog/comment')
                        ->withErrors('Not found comment');
            }
            $input = [
                'approve' => $request->input('item.approve'),
                'content' => $request->input('item.content'),
            ];
            $model = $model->update($input);
            $messages = array('success'=> [
                            'Save comment success!',
                        ],);
            if($request->input('submit_continue')) {
                return redirect(config('base.admin_uri').'/blog/comment/edit/'.$id)
                    ->with('messages',$messages);
            }
            return redirect(config('base.admin_uri').'/blog/comment')
                    ->with('messages',$messages);        
        } catch (Exception $ex) {
            return redirect(config('base.admin_uri').'/blog/comment/edit/'.$id)
                    ->withErrors($ex)->withInput();
        }
    }
    
    public function delete(Request $request, $id, $token)
    {
        if (csrf_token() != $token) {
            return redirect(config('base.admin_uri').'/blog/comment')
                    ->withErrors('Error token key!');
        }
        $model = BlogComment::find($id);
        if(!count($model)) {
            return redirect(config('base.admin_uri').'/blog/comment')
                    ->withErrors('Not found Item');
        }
        try {
            $model->delete();
            $messages = array('success'=> [
                            'Delete item success!',
                        ],);
            return redirect(config('base.admin_uri').'/blog/comment')
                    ->with('messages',$messages);
        } catch (Exception $ex) {
            return redirect(config('base.admin_uri').'/blog/comment')->withErrors($ex);
        }
    }
    
    public function massAction(Request $request)
    {
        if (!count($request->input('item')) || !$request->input('item')) {
            return redirect(config('base.admin_uri').'/blog/comment')->withErrors('Please choose item');
        }
        if ($request->input('action') == 'delete') {
            return $this->massDelete($request);
        }
        return redirect(config('base.admin_uri').'/blog/comment');
    }
    
    protected function massDelete(Request $request)
    {
        try{
            foreach ($request->input('item') as $item) {
                $model = BlogComment::find($item);
                if(count($model)) {
                    $model->delete();
                }
            }
            $messages = array('success'=> [
                            'Mass delete success!',
                        ],);
            return redirect(config('base.admin_uri').'/blog/comment')
                    ->with('messages',$messages);
        } catch (Exception $ex) {
            return redirect(config('base.admin_uri').'/blog/comment')->withErrors($ex);
        }   
    }
}

