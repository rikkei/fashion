<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Validator;
use File;
use App\Model\UrlRewrite;

class UrlRewriteController extends AdminController
{
    protected $breadcrumb;

    /**
     * List category
     *
     * @return void
     */
    public function index()
    {
        $this->breadcrumb['url_rewirte'] = [
            'text'=> 'Url Rewrite',
        ];
        $model = UrlRewrite::getAllGrid();
        return view('admin.url.rewrite',[
            'breadcrumb' => $this->breadcrumb,
            'title' => 'Url Rewrite',
            'pageTitle' => 'Url Rewrite',
            'menuActive' => 'url_rewrite',
            'model' => $model,
        ]);
    }

    /**
     * reindex url
     * 
     * @param Request $request
     * @param type $token
     * @return type
     */
    public function reindex(Request $request, $token)
    {
        if (csrf_token() != $token) {
            return redirect(config('base.admin_uri').'/url/rewrite')->withErrors('Error token key!');
        }
        try {
            $messages = array('success'=> [
                'Redirect url success!',
            ],);
            UrlRewrite::reindex();
            return redirect(config('base.admin_uri').'/url/rewrite/')
                ->with('messages',$messages);
        } catch (Exception $ex) {
            return redirect(config('base.admin_uri').'/url/rewrite/')->withErrors($ex);
        }
    }
}
