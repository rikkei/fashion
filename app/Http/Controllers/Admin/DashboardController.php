<?php
namespace App\Http\Controllers\Admin;

use Auth;

class DashboardController extends AdminController
{
    protected $breadcrumb;

    public function __construct()
    {
        $this->breadcrumb = [
            'home' => [
                'url' => url(config('base.admin_uri')),
                'text' => 'Home',
                'pre_text' => '<i class="fa fa-dashboard"></i>',
            ],
        ];
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function index()
    {
        $this->breadcrumb['dashboard'] = [
            'text'=> 'Dashboard',
        ];
        return view('admin.dashboard.index',[
            'breadcrumb' => $this->breadcrumb,
            'title' => 'Dashboard',
            'pageTitle' => 'Dashboard',
            'menuActive' => 'dashboard',
        ]);
    }
}
