<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Model\BlogCategory;
use Validator;

class BlogCategoryController extends AdminController
{
    protected $breadcrumb;
    
    /**
     * List news
     *
     * @return void
     */
    public function index(Request $request)
    {
        $this->breadcrumb['blog'] = [
            'text'=> 'Blog',
        ];
        $blog = BlogCategory::getAllGrid();
        return view('admin.blog.category.list',[
            'breadcrumb' => $this->breadcrumb,
            'title' => 'Blog Category',
            'pageTitle' => 'Blog Category',
            'menuActive' => 'Blog Category',
            'model' => $blog,
        ]);
    }
    
    /**
     * Create new news
     *
     * @return void
     */
    public function create()
    {
        $this->breadcrumb['blog'] = [
            'text' => 'Blog category',
            'url' => url(config('base.admin_uri').'/blog/category'),
        ];
        $this->breadcrumb['new'] = [
            'text' => 'Add new',
        ];
        return view('admin.blog.category.edit',[
            'breadcrumb' => $this->breadcrumb,
            'title' => 'Add new Blog category',
            'pageTitle' => 'Add new Blog category',
            'menuActive' => 'blog',
            'model' => null,
        ]);
    }
    
    /**
     * Save new news
     *
     * @return void
     */
    public function createPost(Request $request)
    {
        try{
            $input = $request->input('item');
            if(!$input['url_key']) {
                $input['url_key'] = \App\Helpers\Config::urlSlug($input['name']);
            }
            $validator = Validator::make($input, [
                'name' => 'required|max:255',
                'url_key' => 'required|max:255|unique:blog_category',
            ]);
            if ($validator->fails()) {
                return redirect(config('base.admin_uri').'/blog/category/create')
                    ->withInput()
                    ->withErrors($validator);
            }
            $model = BlogCategory::create($input);
            $id = $model->id;
            $messages = array('success'=> [
                            'Create Blog Category success!',
                        ],);
            if($request->input('submit_continue')) {
                return redirect(config('base.admin_uri').'/blog/category/edit/'.$id)
                    ->with('messages',$messages);
            }
            return redirect(config('base.admin_uri').'/blog/category/')
                    ->with('messages',$messages);        
        } catch (Exception $ex) {
            return redirect(config('base.admin_uri').'/blog/category/create')->withErrors($ex)->withInput();
        }
    }
    
    /**
     * Edit category
     *
     * @return void
     */
    public function edit($id)
    {
        $model = BlogCategory::find($id);
        if(!count($model)) {
            return redirect(config('base.admin_uri').'/blog/category')
                    ->withErrors('Not found blog category');
        }
        $this->breadcrumb['blog'] = [
            'text' => 'Blog',
            'url' => url(config('base.admin_uri').'/blog/category'),
        ];
        $this->breadcrumb['edit'] = [
            'text' => $model->name,
        ];
        return view('admin.blog.category.edit',[
            'breadcrumb' => $this->breadcrumb,
            'title' => 'Edit category: '.$model->name,
            'pageTitle' => 'Edit category: '.$model->name,
            'menuActive' => 'blog',
            'model' => $model,
        ]);
    }
    
    /**
     * Save category
     *
     * @return void
     */
    public function editPost(Request $request,$id)
    {
        try{
            $model = BlogCategory::find($id);
            if (!count($model)) {
                return redirect(config('base.admin_uri').'/blog/category')
                        ->withErrors('Not found category');
            }
            $input = $request->input('item');
            if(!$input['url_key']) {
                $input['url_key'] = \App\Helpers\Config::urlSlug($input['name']);
            }
            $validator = Validator::make($input, [
                'name' => 'required|max:255',
                'url_key' => 'required|max:255|unique:blog_category,url_key,'.$id,
                
            ]);
            if ($validator->fails()) {
                return redirect(config('base.admin_uri').'/blog/category/edit/'.$id)
                    ->withInput()
                    ->withErrors($validator);
            }
            $model = $model->update($input);
            $messages = array('success'=> [
                            'Save category success!',
                        ],);
            if($request->input('submit_continue')) {
                return redirect(config('base.admin_uri').'/blog/category/edit/'.$id)
                    ->with('messages',$messages);
            }
            return redirect(config('base.admin_uri').'/blog/category')
                    ->with('messages',$messages);        
        } catch (Exception $ex) {
            return redirect(config('base.admin_uri').'/blog/category/edit/'.$id)
                    ->withErrors($ex)->withInput();
        }
    }
    
    public function delete(Request $request, $id, $token)
    {
        if (csrf_token() != $token) {
            return redirect(config('base.admin_uri').'/blog/category')
                    ->withErrors('Error token key!');
        }
        $model = BlogCategory::find($id);
        if(!count($model)) {
            return redirect(config('base.admin_uri').'/blog/category')
                    ->withErrors('Not found Item');
        }
        try {
            $model->delete();
            $messages = array('success'=> [
                            'Delete item success!',
                        ],);
            return redirect(config('base.admin_uri').'/blog/category')
                    ->with('messages',$messages);
        } catch (Exception $ex) {
            return redirect(config('base.admin_uri').'/blog/category')->withErrors($ex);
        }
    }
    
    public function massAction(Request $request)
    {
        if (!count($request->input('item')) || !$request->input('item')) {
            return redirect(config('base.admin_uri').'/blog/category')->withErrors('Please choose item');
        }
        if ($request->input('action') == 'delete') {
            return $this->massDelete($request);
        }
        return redirect(config('base.admin_uri').'/blog/category');
    }
    
    protected function massDelete(Request $request)
    {
        try{
            foreach ($request->input('item') as $item) {
                $model = BlogCategory::find($item);
                if(count($model)) {
                    $model->delete();
                }
            }
            $messages = array('success'=> [
                            'Mass delete success!',
                        ],);
            return redirect(config('base.admin_uri').'/blog/category')
                    ->with('messages',$messages);
        } catch (Exception $ex) {
            return redirect(config('base.admin_uri').'/blog/category')->withErrors($ex);
        }   
    }
}
