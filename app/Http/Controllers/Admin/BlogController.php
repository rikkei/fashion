<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Model\Blog;
use Validator;

class BlogController extends AdminController
{
    protected $breadcrumb;
    
    /**
     * List news
     *
     * @return void
     */
    public function index(Request $request)
    {
        $this->breadcrumb['blog'] = [
            'text'=> 'Blog',
        ];
        $blog = Blog::getAllGrid();
        return view('admin.blog.list',[
            'breadcrumb' => $this->breadcrumb,
            'title' => 'Blog',
            'pageTitle' => 'Blog',
            'menuActive' => 'Blog',
            'model' => $blog,
        ]);
    }
    
    /**
     * Create new news
     *
     * @return void
     */
    public function create()
    {
        $this->breadcrumb['blog'] = [
            'text' => 'Blog',
            'url' => url(config('base.admin_uri').'/blog'),
        ];
        $this->breadcrumb['new'] = [
            'text' => 'Add new',
        ];
        return view('admin.blog.edit',[
            'breadcrumb' => $this->breadcrumb,
            'title' => 'Add new Blog',
            'pageTitle' => 'Add new Blog',
            'menuActive' => 'blog',
            'model' => null,
        ]);
    }
    
    /**
     * Save new news
     *
     * @return void
     */
    public function createPost(Request $request)
    {
        try{
            $input = $request->input('item');
            if(!$input['url_key']) {
                $input['url_key'] = \App\Helpers\Config::urlSlug($input['name']);
            }
            $validator = Validator::make($input, [
                'name' => 'required|max:255',
                'url_key' => 'required|max:255|unique:blog',
                'created_at' => 'date_format:Y-m-d H:i:s',
                
            ]);
            if ($validator->fails()) {
                return redirect(config('base.admin_uri').'/blog/create')
                    ->withInput()
                    ->withErrors($validator);
            }
            if (isset($input['created_at']) && !$input['created_at']) {
                unset($input['created_at']);
            }
            $model = Blog::create($input);
            $id = $model->id;
            $messages = array('success'=> [
                            'Create Blog success!',
                        ],);
            if($request->input('submit_continue')) {
                return redirect(config('base.admin_uri').'/blog/edit/'.$id)
                    ->with('messages',$messages);
            }
            return redirect(config('base.admin_uri').'/blog/')
                    ->with('messages',$messages);        
        } catch (Exception $ex) {
            return redirect(config('base.admin_uri').'/blog/create')->withErrors($ex)->withInput();
        }
    }
    
    /**
     * Edit category
     *
     * @return void
     */
    public function edit($id)
    {
        $model = Blog::find($id);
        if(!count($model)) {
            return redirect(config('base.admin_uri').'/blog')->withErrors('Not found blog');
        }
        $this->breadcrumb['blog'] = [
            'text' => 'Blog',
            'url' => url(config('base.admin_uri').'/blog'),
        ];
        $this->breadcrumb['edit'] = [
            'text' => $model->name,
        ];
        return view('admin.blog.edit',[
            'breadcrumb' => $this->breadcrumb,
            'title' => 'Edit Blog: '.$model->name,
            'pageTitle' => 'Edit Blog: '.$model->name,
            'menuActive' => 'blog',
            'model' => $model,
        ]);
    }
    
    /**
     * Save category
     *
     * @return void
     */
    public function editPost(Request $request,$id)
    {
        try{
            $model = Blog::find($id);
            if (!count($model)) {
                return redirect(config('base.admin_uri').'/blog')->withErrors('Not found Blog');
            }
            $input = $request->input('item');
            if(!$input['url_key']) {
                $input['url_key'] = \App\Helpers\Config::urlSlug($input['name']);
            }
            $validator = Validator::make($input, [
                'name' => 'required|max:255',
                'url_key' => 'required|max:255|unique:blog,url_key,'.$id,
                'created_at' => 'date_format:Y-m-d H:i:s',
                
            ]);
            if ($validator->fails()) {
                return redirect(config('base.admin_uri').'/blog/edit/'.$id)
                    ->withInput()
                    ->withErrors($validator);
            }
            if (isset($input['created_at']) && !$input['created_at']) {
                unset($input['created_at']);
            }
            $model = $model->update($input);
            $messages = array('success'=> [
                            'Save Blog success!',
                        ],);
            if($request->input('submit_continue')) {
                return redirect(config('base.admin_uri').'/blog/edit/'.$id)
                    ->with('messages',$messages);
            }
            return redirect(config('base.admin_uri').'/blog/')
                    ->with('messages',$messages);        
        } catch (Exception $ex) {
            return redirect(config('base.admin_uri').'/blog/edit/'.$id)->withErrors($ex)->withInput();
        }
    }
    
    public function delete(Request $request, $id, $token)
    {
        if (csrf_token() != $token) {
            return redirect(config('base.admin_uri').'/blog/')->withErrors('Error token key!');
        }
        $model = Blog::find($id);
        if(!count($model)) {
            return redirect(config('base.admin_uri').'/blog')->withErrors('Not found Item');
        }
        try {
            $model->delete();
            $messages = array('success'=> [
                            'Delete item success!',
                        ],);
            return redirect(config('base.admin_uri').'/blog/')
                    ->with('messages',$messages);
        } catch (Exception $ex) {
            return redirect(config('base.admin_uri').'/blog/')->withErrors($ex);
        }
    }
    
    public function massAction(Request $request)
    {
        if (!count($request->input('item')) || !$request->input('item')) {
            return redirect(config('base.admin_uri').'/blog/')->withErrors('Please choose item');
        }
        if ($request->input('action') == 'delete') {
            return $this->massDelete($request);
        }
        return redirect(config('base.admin_uri').'/blog/');
    }
    
    protected function massDelete(Request $request)
    {
        try{
            foreach ($request->input('item') as $item) {
                $model = Blog::find($item);
                if(count($model)) {
                    $model->delete();
                }
            }
            $messages = array('success'=> [
                            'Mass delete success!',
                        ],);
            return redirect(config('base.admin_uri').'/blog/')
                    ->with('messages',$messages);
        } catch (Exception $ex) {
            return redirect(config('base.admin_uri').'/blog/')->withErrors($ex);
        }   
    }
}
