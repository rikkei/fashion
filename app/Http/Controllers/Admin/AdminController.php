<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    protected $guard = 'admin';
    
    public function __construct()
    {
        $this->breadcrumb = [
            'home' => [
                'url' => url(config('base.admin_uri')),
                'text' => 'Home',
                'pre_text' => '<i class="fa fa-dashboard"></i>',
            ],
        ];
    }
}
