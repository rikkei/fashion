<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//frontend Area
Route::auth();
Route::group([
    'namespace' => 'Frontend',
], function () {
    //home page
    Route::get('/home', 'HomeController@index');
    Route::get('/', 'HomeController@index');
    
    //category page
    Route::get('/category/{id}', 'CategoryController@index')->where('id', '[0-9]+')->name('category');
    
    //product page
    Route::get('/product/{id}', 'ProductController@index')->where('id', '[0-9]+')->name('product');
    
    //article view
    Route::get('/news', 'NewsController@index');
    Route::get('/news/{id}', 'NewsController@view')->where('id', '[0-9]+')->name('news');
    
    //cart
    Route::group([
        'prefix' => 'cart',
    ], function() {
        Route::get('/', 'CartController@index');
        Route::post('/add','CartController@add');
        Route::get('/delete/{id}/{token}','CartController@delete')->where('id', '[0-9]+');
        Route::post('/checkout', 'CartController@checkout');
        Route::get('/success', 'CartController@success');
    });
    
});

//Admin area
$adminUri = config('base.admin_uri');
Route::group([
    'namespace' => 'Adminauth',
    'prefix' => $adminUri,
], function () {
    //Route login
    Route::get('/login','AuthController@loginView');
    Route::post('/login','AuthController@login');
    
    //Route reset pass
    Route::get('/password/reset','PasswordController@resetPassword');
    
    //Route register
/*    Route::get('/register', 'AuthController@registerView');
    Route::post('/register', 'AuthController@register');*/
    
    //Route logout
    Route::get('/logout','AuthController@logout');
});

Route::group([
    'middleware' => 'admin',
    'prefix' => $adminUri,
    'namespace' => 'Admin'
], function () {
    //Dashboard 
    Route::get('/', 'DashboardController@index');
    
    //Category Manage
    Route::group(['prefix' => 'category'], function () {
        Route::get('/','CategoryController@index');
//        Route::get('/create','CategoryController@create');
        Route::post('/create','CategoryController@createPost');
        Route::get('/edit/{id}','CategoryController@edit')->where('id', '[0-9]+');
        Route::post('/edit/{id}','CategoryController@editPost');
        Route::get('/delete/{id}/{token}','CategoryController@delete')->where('id', '[0-9]+');
        Route::post('/massAction','CategoryController@massAction');
    });
    
    //Product Manage
    Route::group(['prefix' => 'product'], function () {
        Route::get('/','ProductController@index');
        Route::get('/create','ProductController@create');
        Route::post('/create','ProductController@createPost');
        Route::get('/edit/{id}','ProductController@edit')->where('id', '[0-9]+');
        Route::post('/edit/{id}','ProductController@editPost');
        Route::get('/delete/{id}/{token}','ProductController@delete')->where('id', '[0-9]+');
        Route::post('/massAction','ProductController@massAction');
    });
    
    //order manager
    Route::get('/order', 'OrderController@index');
    Route::get('/order/edit/{id}', 'OrderController@edit')->where('id', '[0-9]+');
    
    //News Manage
    Route::group(['prefix' => 'news'], function () {
        Route::get('/','NewsController@index');
        Route::get('/create','NewsController@create');
        Route::post('/create','NewsController@createPost');
        Route::get('/edit/{id}','NewsController@edit')->where('id', '[0-9]+');
        Route::post('/edit/{id}','NewsController@editPost');
        Route::get('/delete/{id}/{token}','NewsController@delete')->where('id', '[0-9]+');
        Route::post('/massAction','NewsController@massAction');
    });
    
    //Blog Manage
    Route::group(['prefix' => 'blog'], function () {
        Route::get('/','BlogController@index');
        Route::get('/create','BlogController@create');
        Route::post('/create','BlogController@createPost');
        Route::get('/edit/{id}','BlogController@edit')->where('id', '[0-9]+');
        Route::post('/edit/{id}','BlogController@editPost');
        Route::get('/delete/{id}/{token}','BlogController@delete')->where('id', '[0-9]+');
        Route::post('/massAction','BlogController@massAction');
        
        //blog category
        Route::group(['prefix' => 'category'], function () {
            Route::get('/','BlogCategoryController@index');
            Route::get('/create','BlogCategoryController@create');
            Route::post('/create','BlogCategoryController@createPost');
            Route::get('/edit/{id}','BlogCategoryController@edit')->where('id', '[0-9]+');
            Route::post('/edit/{id}','BlogCategoryController@editPost');
            Route::get('/delete/{id}/{token}','BlogCategoryController@delete')->where('id', '[0-9]+');
            Route::post('/massAction','BlogCategoryController@massAction');
        });
        
        //blog comment
        Route::group(['prefix' => 'comment'], function () {
            Route::get('/','BlogCommentController@index');
            Route::get('/create','BlogCommentController@create');
            Route::post('/create','BlogCommentController@createPost');
            Route::get('/edit/{id}','BlogCommentController@edit')->where('id', '[0-9]+');
            Route::post('/edit/{id}','BlogCommentController@editPost');
            Route::get('/delete/{id}/{token}','BlogCommentController@delete')->where('id', '[0-9]+');
            Route::post('/massAction','BlogCommentController@massAction');
        });
    });
    
    //Url rewirte
    Route::get('/url/rewrite', 'UrlRewriteController@index');
    Route::get('/url/rewrite/reindex/{token}', 'UrlRewriteController@reindex');
    
});

//Route Ckeditor
Route::get('ckeditor/browse','Plugin\CkeditorController@browse');
Route::get('ckeditor/upload','Plugin\CkeditorController@upload');

//404 Page
Route::get('404','Controller@page404')->name('404');