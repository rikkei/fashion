<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('url_key');
            $table->string('thumbnail');
            $table->string('image');
            $table->text('short_description');
            $table->text('description');
            $table->boolean('active');
            $table->integer('user_id');
            $table->string('user_name');
            $table->integer('blog_category_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('blog');
    }
}
