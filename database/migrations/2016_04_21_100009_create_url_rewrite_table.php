<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUrlRewriteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('url_rewrite', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type');
            $table->string('request_path');
            $table->string('target_path');
            $table->string('item_id');
            $table->string('params');
            $table->boolean('redirect');
            $table->unique('request_path');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('order_item');
    }
}
