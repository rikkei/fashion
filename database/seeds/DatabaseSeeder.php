<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createAdminAccount();
        $this->createAttribute();
    }
    
    /**
     * Create admin account
     * 
     * @return type
     */
    protected function createAdminAccount()
    {
        $admin = DB::table('admin')->where('email','admin@gmail.com')->get();
        if(count($admin)) {
            return;
        }
        DB::table('admin')->insert([
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('admin123'),
        ]);
        echo 'Create admin account success!';
        echo PHP_EOL;
        return $this;
    }
    
    /**
     * Create attribute product
     * 
     * 
     */
    protected function createAttribute()
    {
        $attributes = [
            'name' => array(
                'label' => 'Name',
                'required' => '1',
                'unique' => '0',
            ),
            'url_key' => array(
                'label' => 'Url Key',
                'required' => '1',
                'unique' => '0',
            ),
            'price' => array(
                'label' => 'Price',
                'required' => '1',
                'unique' => '0',
            ),
            'special_price' => array(
                'label' => 'Special Price',
                'required' => '0',
                'unique' => '0',
            ),
            'special_price_from' => array(
                'label' => 'Special Price From Date',
                'required' => '0',
                'unique' => '0',
            ),
            'special_price_to' => array(
                'label' => 'Special Price To Date',
                'required' => '0',
                'unique' => '0',
            ),
            'price' => array(
                'label' => 'Price',
                'required' => '1',
                'unique' => '0',
            ),
            'price' => array(
                'label' => 'Price',
                'required' => '0',
                'unique' => '0',
            ),
            'short_description' => array(
                'label' => 'Short Description',
                'required' => '0',
                'unique' => '0',
            ),
            'description' => array(
                'label' => 'Description',
                'required' => '0',
                'unique' => '0',
            ),
            'description' => array(
                'label' => 'Description',
                'required' => '0',
                'unique' => '0',
            ),
            'image' => array(
                'label' => 'Image',
                'required' => '0',
                'unique' => '0',
            ),
            'qty' => array(
                'label' => 'Qty',
                'required' => '1',
                'unique' => '0',
            ),
            'stock_status' => array(
                'label' => 'Stock Status',
                'required' => '1',
                'unique' => '0',
            ),
            
        ];
        foreach ($attributes as $code => $attributeInfo) {
            $attribute = DB::table('attribute')->where('code',$code)->get();
            if(count($attribute)) {
                continue;
            }
            DB::table('attribute')->insert([
                'code' => $code,
                'label' => $attributeInfo['label'],
                'required' => $attributeInfo['required'],
                'unique' => $attributeInfo['unique'],
            ]);
            echo 'Create attribute ' . $code .' success!';
            echo PHP_EOL;
        }
        return $this;
    }
}
