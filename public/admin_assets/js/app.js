jQuery(document).ready(function($) {
    // Delete confirm click
    $(document).on('click','.delete-confirm',function(event) {
        var message = $(this).data('message');
        if(!message) {
            message = 'Are you sure delete item?';
        }
        if(!confirm(message)) {
            event.preventDefault();
        }
    });
    
    //action process form pager submit
    $(document).on('submit','form.form-pager',function(event) {
        event.preventDefault();
        var serializeForm = $(this).serialize();
        var action = $(this).attr('action');
        window.location.href = action +'&'+ serializeForm;
    });
    
    //check before submit mass action
    $(document).on('submit','form.form-grid-actions',function(event) {
        var data = {};
        $(this).serializeArray().map(function(x){data[x.name] = x.value;});
        if(data['action'] === undefined || !data['action']) {
            alert('Please choose action!');
            event.preventDefault();
        } else if(data['item[]'] === undefined) {
            alert('Please choose item!');
            event.preventDefault();
        } else if(!confirm('Are you sure mass action!')) {
            event.preventDefault();
        }
    });
    
    // get params from filter
    function getSerializeFilter()
    {
        var valueFilter, nameFilter, params;
        params = '';
        $('.filter-grid').each(function(i,k) {
            valueFilter = $(this).val();
            nameFilter = $(this).attr('name');
            if (valueFilter && nameFilter) {
                params += nameFilter + '=' + valueFilter + '&';
            }
        });
        return params;
    }
    
    //filter request redirect with param
    function filterRequest()
    {
        var url = $('.btn-reset-filter').data('href'),
            params = getSerializeFilter();
        if (url && params) {
            window.location.href = url + '?' + params;
        }
    }
    
    //input key down - disable submit form - redirect action filter
    $(document).on('keydown','input.filter-grid',function(event) {
        if(event.which == 13) {
            filterRequest();
            return false;
        }
    });
    
    //reset filter
    $(document).on('click','.btn-reset-filter',function(event) {
        if ($(this).data('href')) {
            window.location.href = $(this).data('href');
        }
        return false;
    });
    
    //search filter button
    $(document).on('click','.btn-search-filter',function(event) {
        filterRequest();
        return false;
    });
    
    //select box all
    $(document).on('change','input.mass-selectbox[type=checkbox]',function(event) {
        if ($(this).is(':checked')) {
            $('input.mass-item[type=checkbox]').prop('checked',true);
        } else {
            $('input.mass-item[type=checkbox]').prop('checked',false);
        }
    });
});