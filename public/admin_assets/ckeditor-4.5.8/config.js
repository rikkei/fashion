/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
    config.language = 'en';
    
    config.filebrowserBrowseUrl = baseUrl + 'ckeditor/browse?type=Files';
    //config.filebrowserUploadUrl = baseUrl + 'ckeditor/upload?type=Files';
    config.filebrowserImageBrowseUrl = baseUrl + 'ckeditor/browse?type=Images';
    config.filebrowserFlashBrowseUrl = baseUrl + 'ckeditor/browse?type=Flash';
    //config.filebrowserUploadUrl = 'http://localhost/fckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
    //config.filebrowserImageUploadUrl = 'http://localhost/fckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
    //config.filebrowserFlashUploadUrl = 'http://localhost/fckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';
};
