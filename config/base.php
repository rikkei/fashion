<?php
/**
 * add base config - custom
 */
return [
    'admin_uri' => 'admin',
    
    'product_image' => '/media/product/default.png',
];
